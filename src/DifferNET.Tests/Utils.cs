﻿using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DifferNET.Tests
{
	public class Utils
	{
		public static void AssertEqualish(double actual, double expected, string message)
		{
			const double d = 1.0 / (1024 * 1024);
			Assert.That(actual, Is.InRange(expected - d, expected + d), message);
		}

		public static void AssertEqualish(double[] actual, double[] expected, string message)
		{
			const double d = 1.0 / (1024 * 1024);

			Assert.That(actual.Length, Is.EqualTo(expected.Length), "Lengths not equal");

			for (int i = 0; i < actual.Length; i++)
			{
				Assert.That(
					actual[i], 
					Is.InRange(expected[i] - d, expected[i] + d), 
					message + ": Differ at index " + i);
			}
		}

		
	}
}
