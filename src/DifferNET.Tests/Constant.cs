﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;
using System.Linq;

namespace DifferNET.Tests
{
	[TestFixture]
	public class ConstantTests
	{
		[TestCase(0d)]
		[TestCase(7.4d)]
		[TestCase(-4.7d)]
		[TestCase(double.MinValue)]
		[TestCase(double.MaxValue)]
		[TestCase(double.PositiveInfinity)]
		[TestCase(double.NegativeInfinity)]
		[TestCase(double.NaN)]
		public void Constant_(double d)
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				x => Constant.Of(d),
				x => d,
				x => 0d);
		}

		[Test]
		public void Precendents()
		{
			Input x = new Input("x");

			Expression expression = Constant.Of(42.3);

			Expression[] actual = expression.Precedents;
			Expression[] expected = { };

			Assert.That(actual, Is.EquivalentTo(expected));
		}
	}
}
