﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;
using System.Linq;

namespace DifferNET.Tests
{
	[TestFixture]
	public class GeneralTests
	{
		[Test]
		public void CompileWithNoInputs()
		{
			Expression expression = Constant.Of(2) + Constant.Of(3);

			var compiled = expression.Compile();

			Assert.That(compiled, Is.Not.Null);
			Assert.That(compiled.Invoke(), Is.EqualTo(5));
			Assert.That(compiled.Invoke(null), Is.EqualTo(5));
		}

		[Test]
		public void DifferentiateWithNoInputs()
		{
			Expression expression = Constant.Of(2) + Constant.Of(3);

			Assert.That(() => expression.GetDerivatives(null), Throws.Exception);
			Assert.That(() => expression.GetDerivatives(), Throws.Exception);
		}
	}
}
