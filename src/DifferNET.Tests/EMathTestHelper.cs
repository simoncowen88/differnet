﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DifferNET.Tests
{
	public static class EMathTestHelper
	{
		//private static readonly Random rand = new Random();

		// TODO: we should also do some numerical checks on the gradient I think...
		// awkward for non-continuous things though... hmmm....

		public static void AssertCompiledAndDerivative(
			Func<Input, Expression> expressionFunc,
			Func<double, double> checkFunc,
			Func<double, double> derivativeCheckFunc)
		{
			AssertEquivalent(
				expressionFunc,
				checkFunc,
				"/Func");

			AssertDerivativeEquivalent(
				expressionFunc,
				derivativeCheckFunc,
				"/Deriv");
		}

		public static void AssertCompiledAndDerivative(
			Func<Input, Input, Expression> expressionFunc,
			Func<double, double, double> checkFunc,
			Func<double, double, double> derivativeCheckFunc0,
			Func<double, double, double> derivativeCheckFunc1)
		{
			AssertEquivalent(
				expressionFunc, 
				checkFunc,
				"/Func");

			AssertDerivativeEquivalent(
				expressionFunc,
				derivativeCheckFunc0,
				derivativeCheckFunc1,
				"/Deriv");
		}
		public static void AssertCompiledAndDerivative(
			Func<Input, double, Expression> expressionFunc,
			Func<double, double, double> checkFunc,
			Func<double, double, double> derivativeCheckFunc)
		{
			AssertEquivalent(
				expressionFunc,
				checkFunc,
				"/Func");

			AssertDerivativeEquivalent(
				expressionFunc,
				derivativeCheckFunc,
				"/Deriv");
		}

		public static void AssertDerivativeEquivalent(
			Func<Input, Expression> expressionFunc,
			Func<double, double> derivativeCheckFunc,
			string message)
		{
			var x = new Input("x");
			var expression = expressionFunc(x);

			var derivative = expression.GetDerivative(x);
			var compiledDerivative = derivative.Compile(x);

			var directlyCompiledDerivative = expression.CompileDerivative(x);

			AssertEquivalent(
				compiledDerivative.Invoke,
				derivativeCheckFunc,
				message + "/CompiledDeriv");

			AssertEquivalent(
				directlyCompiledDerivative.Invoke,
				derivativeCheckFunc,
				message + "/DirectlyCompiledDeriv");
		}
		public static void AssertDerivativeEquivalent(
			Func<Input, Input, Expression> expressionFunc,
			Func<double, double, double> derivativeCheckFunc0,
			Func<double, double, double> derivativeCheckFunc1,
			string message)
		{
			var x = new Input("x");
			var y = new Input("y");
			var expression = expressionFunc(x, y);

			var derivatives = expression.GetDerivatives(x, y);
			var compiledDerivativeX = derivatives[x].Compile(x, y);
			var compiledDerivativeY = derivatives[y].Compile(x, y);

			var directlyCompiledDerivative = expression.CompileDerivatives(x, y);

			AssertEquivalent(
				compiledDerivativeX.Invoke,
				derivativeCheckFunc0,
				message + "/CompiledDeriv/wrtX");

			AssertEquivalent(
				compiledDerivativeY.Invoke,
				derivativeCheckFunc1,
				message + "/CompiledDeriv/wrtY");

			AssertEquivalent(
				o => directlyCompiledDerivative.Invoke(o)[0],
				derivativeCheckFunc0,
				message + "/DirectlyCompiledDeriv/wrtX");
			AssertEquivalent(
				o => directlyCompiledDerivative.Invoke(o)[1],
				derivativeCheckFunc1,
				message + "/DirectlyCompiledDeriv/wrtY");
		}
		public static void AssertDerivativeEquivalent(
			Func<Input, double, Expression> expressionFunc,
			Func<double, double, double> derivativeCheckFunc,
			string message)
		{
			var x = new Input("x");

			foreach (var y in GetTestCases())
			{
				var expression = expressionFunc(x, y);

				var derivative = expression.GetDerivative(x);
				var compiledDerivative = derivative.Compile(x);

				var directlyCompiledDerivative = expression.CompileDerivative(x);

				AssertEquivalent(
					compiledDerivative.Invoke, 
					a => derivativeCheckFunc(a, y),
					message + "/CompiledDeriv" + "/(" + y + ")");

				AssertEquivalent(
					directlyCompiledDerivative.Invoke,
					a => derivativeCheckFunc(a, y),
					message + "/DirectlyCompiledDeriv" + "/(" + y + ")");
			}
		}

		public static void AssertEquivalent(
			Func<Input, Expression> expressionFunc,
			Func<double, double> checkFunc,
			string message)
		{
			var x = new Input("x");
			var expression = expressionFunc(x);
			var compiled = expression.Compile(x);

			AssertEquivalent(compiled.Invoke, checkFunc, message);
		}
		public static void AssertEquivalent(
			Func<Input, Input, Expression> expressionFunc,
			Func<double, double, double> checkFunc,
			string message)
		{
			var x = new Input("x");
			var y = new Input("y");
			var expression = expressionFunc(x, y);
			var compiled = expression.Compile(x, y);

			AssertEquivalent(compiled.Invoke, checkFunc, message);
		}
		public static void AssertEquivalent(
			Func<Input, double, Expression> expressionFunc,
			Func<double, double, double> checkFunc,
			string message)
		{
			var x = new Input("x");

			foreach (var y in GetTestCases())
			{
				var expression = expressionFunc(x, y);
				var compiled = expression.Compile(x);

				AssertEquivalent(compiled.Invoke, (a) => checkFunc(a, y),
					message + "/(" + y + ")");
			}
		}

		private static void AssertEquivalent(
			Func<double, double> expressionFunc,
			Func<double, double> checkFunc, 
			string message)
		{
			foreach (var a in GetTestCases())
			{
				double actual = expressionFunc(a);
				double expected = checkFunc(a);

				Utils.AssertEqualish(actual, expected, message + "/(" + a + ")");
			}
		}
		private static void AssertEquivalent(
			Func<double[], double> expressionFunc,
			Func<double, double, double> checkFunc, 
			string message)
		{
			foreach (var a in GetTestCases())
				foreach (var b in GetTestCases())
				{
					double actual = expressionFunc(new[] { a, b });
					double expected = checkFunc(a, b);

					Utils.AssertEqualish(actual, expected, message + "/(" + a + "," + b + ")");
				}
		}

		private static IEnumerable<double> GetTestCases()
		{
			return _GetTestCases().ToList();
		}
		private static IEnumerable<double> _GetTestCases()
		{
			yield return 0.0;
			yield return -0.0349;
			yield return -4.7621;
			yield return -6571.494362;
			yield return -9478245.43278432;
			yield return 0.0723;
			yield return 6.4381;
			yield return 92645.394632;
			yield return 25687678.02745433;
			yield return double.NaN;
			yield return double.PositiveInfinity;
			yield return double.NegativeInfinity;
			yield return double.MinValue;
			yield return double.MaxValue;
		}
		private static IEnumerable<double> GetTestCases1()
		{
			foreach (var item in GetTestCases())
				yield return item;
		}
		private static IEnumerable<Tuple<double, double>> GetTestCases2()
		{
			foreach (var a in GetTestCases())
				foreach (var b in GetTestCases())
					yield return Tuple.Create(a, b);
		}

		//private static double GetRand()
		//{
		//	double d = rand.NextDouble(); // [0,1]
		//	d = (d * 2d) - 1d; // [-1,1]
		//	return d * double.MaxValue;
		//}

		//private static double GetRand(double minimum, double maximum)
		//{
		//	double d = rand.NextDouble(); // [0,1]
		//	return d * (maximum - minimum) + minimum;
		//}
	}
}
