﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;
using System.Linq;

namespace DifferNET.Tests
{
	[TestFixture]
	public class InputTests
	{
		[Test]
		public void Input_()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				x => x,
				x => x,
				x => 1d);
		}

		[Test]
		public void Precendents()
		{
			var x = new Input("x");

			var actual = x.Precedents.ToArray();
			var expected = new Expression[0];

			Assert.That(actual, Is.EquivalentTo(expected));
		}
	}
}