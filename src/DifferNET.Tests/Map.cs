﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;

namespace DifferNET.Tests
{
	[TestFixture]
	public class MapTests
	{
		#region helpers

		public abstract class TestBase
		{
			internal Dictionary<int, string> dict;

			[SetUp]
			public virtual void SetUp()
			{
				this.dict = new Dictionary<int, string>
				{
					{1, "one"},
					{2, "two"},
					{3, "three"}
				};
			}

			internal Map<int, string> WithThings(Map<int, string> map)
			{
				return map
					.With(1, "one")
					.With(2, "two")
					.With(3, "three");
			}

			internal Map<int, string> GetTarget()
			{
				return new Map<int, string>();
			}
		}

		#endregion

		[TestFixture]
		public class Constructor : TestBase
		{
			[Test]
			public void CanConstruct()
			{
				var map = new Map<int, string>();
				Assert.Pass();
			}

			[Test]
			public void CanConstructWithDict()
			{
				var map = new Map<int, string>(dict);

				Assert.That(map[1], Is.EqualTo("one"));
				Assert.That(map[2], Is.EqualTo("two"));
				Assert.That(map[3], Is.EqualTo("three"));
			}
		}

		[TestFixture]
		public class With : TestBase
		{
			[Test]
			public void EmptyWithItem()
			{
				var map = new Map<int, string>();
				var mapWithItem = map.With(1, "one");

				Assert.That(mapWithItem, Is.Not.SameAs(map));
				Assert.That(mapWithItem[1], Is.EqualTo("one"));
				Assert.That(() => map[1], Throws.InstanceOf<KeyNotFoundException>());
			}

			[Test]
			public void WithNewItem()
			{
				var map = WithThings(new Map<int, string>());
				var mapWithItem = map.With(4, "four");

				Assert.That(mapWithItem, Is.Not.SameAs(map));
				Assert.That(mapWithItem[1], Is.EqualTo("one"));
				Assert.That(mapWithItem[2], Is.EqualTo("two"));
				Assert.That(mapWithItem[3], Is.EqualTo("three"));
				Assert.That(mapWithItem[4], Is.EqualTo("four"));
				Assert.That(() => map[4], Throws.InstanceOf<KeyNotFoundException>());
			}

			[Test]
			public void WithChangedItem()
			{
				var map = WithThings(new Map<int, string>());
				var mapWithItem = map.With(3, "threeeee");

				Assert.That(mapWithItem, Is.Not.SameAs(map));
				Assert.That(mapWithItem[1], Is.EqualTo("one"));
				Assert.That(mapWithItem[2], Is.EqualTo("two"));
				Assert.That(mapWithItem[3], Is.EqualTo("threeeee"));
				Assert.That(map[3], Is.EqualTo("three"));
			}
		}


		[TestFixture]
		public class Indexer : TestBase
		{
			private static void AssertThings(Map<int, string> map)
			{
				Assert.That(map[1], Is.EqualTo("one"));
				Assert.That(map[2], Is.EqualTo("two"));
				Assert.That(map[3], Is.EqualTo("three"));

				Assert.That(() => map[4], Throws.InstanceOf<KeyNotFoundException>());
			}

			[Test]
			public void FromAdd()
			{
				var map = WithThings(new Map<int, string>());

				AssertThings(map);
			}

			[Test]
			public void PassedDict()
			{
				var map = new Map<int, string>(dict);

				AssertThings(map);
			}
		}

		[TestFixture]
		public class TryGetValue : TestBase
		{
			private static void AssertThings(Map<int, string> map)
			{
				string value;
				Assert.That(map.TryGetValue(1, out value), Is.True);
				Assert.That(value, Is.EqualTo("one"));

				Assert.That(map.TryGetValue(2, out value), Is.True);
				Assert.That(value, Is.EqualTo("two"));

				Assert.That(map.TryGetValue(3, out value), Is.True);
				Assert.That(value, Is.EqualTo("three"));

				Assert.That(map.TryGetValue(4, out value), Is.False);
			}

			[Test]
			public void FromAdd()
			{
				var map = WithThings(new Map<int, string>());

				AssertThings(map);
			}

			[Test]
			public void PassedDict()
			{
				var map = new Map<int, string>(dict);

				AssertThings(map);
			}
		}
	}
}
