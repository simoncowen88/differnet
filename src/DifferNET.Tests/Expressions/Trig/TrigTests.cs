﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;

namespace DifferNET.Tests
{
	[TestFixture]
	public class TrigTests
	{
		[Test]
		public void Sin()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Sin,
				Math.Sin,
				x => Math.Cos(x));
		}

		[Test]
		public void Cos()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Cos,
				Math.Cos,
				x => -Math.Sin(x));
		}

		[Test]
		public void Tan()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Tan,
				Math.Tan,
				x => 1d / (Math.Cos(x) * Math.Cos(x)));
		}

		[Test]
		public void Sinh()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Sinh,
				Math.Sinh,
				x => Math.Cosh(x));
		}

		[Test]
		public void Cosh()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Cosh,
				Math.Cosh,
				x => Math.Sinh(x));
		}

		[Test]
		public void Tanh()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Tanh,
				Math.Tanh,
				x => 1d - Math.Pow(Math.Tanh(x), 2));
		}
	}
}
