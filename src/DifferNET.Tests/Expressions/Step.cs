﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;
using System.Linq;

namespace DifferNET.Tests
{
	[TestFixture]
	public class StepTests
	{
		[TestFixture]
		public class Apply
		{
			[TestCase(0d, 0d, 0d, 0d, 0d)]
			[TestCase(0d, 1d, 0d, 0d, 1d)]
			[TestCase(0d, 1d, 2d, 0d, 1d)]
			[TestCase(0d, 1d, 2d, 1d, 2d)]
			[TestCase(0d, 1d, 2d, -1d, 0d)]
			[TestCase(2d, 1d, 0d, 1d, 0d)]
			[TestCase(2d, 1d, 0d, -1d, 2d)]
			[TestCase(0d, double.PositiveInfinity, 0d, 0d, double.PositiveInfinity)]
			[TestCase(0d, 0d, 0d, double.NaN, double.NaN)]
			[TestCase(0d, 1d, 0d, double.NaN, double.NaN)]
			[TestCase(0d, 1d, 2d, double.NaN, double.NaN)]
			[TestCase(2d, 1d, 0d, double.NaN, double.NaN)]
			public void Tests(
				double leftValue,
				double midValue,
				double rightValue,
				double input,
				double expectedOutput)
			{
				var actualOutput = Step.Apply(input, leftValue, midValue, rightValue);

				Assert.That(actualOutput, Is.EqualTo(expectedOutput));
			}
		}
	}
}
