﻿using System;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;

namespace DifferNET.Tests
{
	[TestFixture]
	public class ExpressionsTests
	{
		//[Test] public void Sum_Multiple() { }

		// all up/down possibilities
		[TestCase(1d, 1d, 1d)]

		[TestCase(1d, 2d, 3d)]
		[TestCase(1d, 2d, 2d)]
		[TestCase(1d, 1d, 2d)]

		[TestCase(3d, 2d, 1d)]
		[TestCase(2d, 2d, 1d)]
		[TestCase(2d, 1d, 1d)]

		[TestCase(2d, 1d, 3d)]
		[TestCase(1d, 3d, 2d)]
		public void Step(double a, double b, double c)
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				x => EMath.Step(x, a, b, c),
				x => StepFunc(x, a, b, c),
				x => StepFuncDerivative(x, a, b, c));
		}
		private static double StepFunc(double x,
			double leftValue, double midValue, double rightValue)
		{
			if (x == 0d) return midValue;
			if (x < 0d) return leftValue;
			if (x > 0d) return rightValue;
			return double.NaN;
		}
		private static double StepFuncDerivative(double x,
			double leftValue, double midValue, double rightValue)
		{
			// ---
			if (leftValue == midValue && midValue == rightValue)
				return 0d;

			// _-' // _-- // __-
			if (leftValue <= midValue && midValue <= rightValue) // e.g. Heavyside
				return StepFunc(x, 0d, double.PositiveInfinity, 0d);

			// '-_ // --_ // -__
			if (leftValue >= midValue && midValue >= rightValue) // e.g. -Heavyside
				return StepFunc(x, 0d, double.NegativeInfinity, 0d);

			// -_- // _-_
			return StepFunc(x, 0d, double.NaN, 0d);
		}

		[Test]
		public void Sum_ExpressionExpression()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Sum,
				(x, y) => x + y,
				(x, y) => 1d,
				(x, y) => 1d);
		}
		
		[Test]
		public void Abs()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Abs,
				Math.Abs,
				AbsDerivative);
		}

		private static double AbsDerivative(double x)
		{
			if (double.IsNaN(x))
				return double.NaN;

			if (x == 0d) 
				return double.NaN;

			if (x > 0d) 
				return 1d;

			return -1d;
		}

		[Test]
		public void Division()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Divide,
				(x, y) => x / y,
				(x, y) => 1d / y,
				(x, y) => -x / (y * y));
		}

		[Test]
		public void Exp()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Exp,
				Math.Exp,
				Math.Exp);
		}

		[Test]
		public void Log_WithSimpleBase()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Log,
				Math.Log,
				(x, y) => 1d / (x * Math.Log(y)));
		}

		[Test]
		public void Log_WithBase()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Log,
				Math.Log,
				(x, y) => 1d / (x * Math.Log(y)),
				(x, y) => -Math.Log(x) / (y * Math.Log(y) * Math.Log(y)));

			//(x, y) => -Math.Log(x) / (Math.Log(y) * Math.Log(y) * y));
			// for some reason, using this as derivWrtY fails for (0, double.MaxValue)
			// it gives Infinity rather than NaN
		}

		[Test]
		public void Log()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Log,
				Math.Log,
				x => 1d / x);
		}

		[Test]
		public void Subtract()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Subtract,
				(x, y) => x - y,
				(x, y) => 1d,
				(x, y) => -1d);
		}

		[Test]
		public void Negate()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Negate,
				x => -x,
				x => -1d);
		}

		[Test]
		public void Pow()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Pow,
				Math.Pow,
				(x, y) => y * Math.Pow(x, y - 1),
				(x, y) => Math.Pow(x, y) * Math.Log(x));
		}

		[Test]
		public void Pow_Simple()
		{
			EMathTestHelper.AssertCompiledAndDerivative(
				EMath.Pow,
				Math.Pow,
				(x, y) => y * Math.Pow(x, y - 1));
		}
	}
}
