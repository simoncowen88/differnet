﻿using System;
using DifferNET.Compiled;
using NUnit;
using Moq;
using DifferNET;
using NUnit.Framework;
using System.Linq;
using System.Collections.Generic;
using LX = System.Linq.Expressions;

namespace DifferNET.Tests
{
	[TestFixture]
	public class ExpressionTests
	{
		#region helpers

		// We do this jiggery-pokery to be able to mock protected methods
		internal class ExpressionFake : Expression
		{
			private readonly Mock<ExpressionFake> mockExpression;

			internal ExpressionFake()
			{
				this.mockExpression = new Mock<ExpressionFake>(MockBehavior.Strict);
			}

			internal Mock<ExpressionFake> Mock
			{
				get { return this.mockExpression; }
			}

			internal override TReturn Accept<TReturn, TParameter>(
				IExpressionVisitor<TReturn, TParameter> visitor,
				TParameter parameter)
			{
				throw new Exception();
			}
			
			// --
			internal virtual IEnumerable<ExpressionDerivative> GetDerivativesCore_()
			{
				throw new NotImplementedException();
			}
			internal virtual LX.Expression GetLinqExpressionCore_()
			{
				throw new NotImplementedException();
			}
		}

		public abstract class TestBase
		{
			internal ExpressionFake expression;

			[SetUp]
			public virtual void SetUp()
			{
				expression = new ExpressionFake();
			}

			[TearDown]
			public virtual void TearDown()
			{
				CompilerFactory.SetFunction1To1CompilerToDefault();
				CompilerFactory.SetFunctionManyTo1CompilerToDefault();
			}
		}

		#endregion

		[TestFixture]
		public class Constructor : TestBase
		{
			[Test]
			public void CanConstruct()
			{
				var expression = new ExpressionFake();

				Assert.Pass();
			}
		}

		// Here, we are mostly tesing the it calls the Core methods correctly
		// We will test that those Core methods are correct in each subclass.

		[TestFixture]
		public class Compile_Input : TestBase
		{
			internal Input input1;
			internal Mock<ICompiler<IFunction1To1>> mockF1To1Compiler;
			internal Mock<IFunction1To1> mockF1To1;

			public override void SetUp()
			{
				base.SetUp();

				input1 = new Input("x");
				mockF1To1Compiler = new Mock<ICompiler<IFunction1To1>>(MockBehavior.Strict);
				mockF1To1 = new Mock<IFunction1To1>(MockBehavior.Strict);

				CompilerFactory.SetFunction1To1Compiler(mockF1To1Compiler.Object);

				mockF1To1Compiler
					.Setup(o => o.GetFunction(It.IsAny<Input[]>(), It.IsAny<Expression[]>()))
					.Returns(mockF1To1.Object);
			}

			[Test]
			public void TestTestTest()
			{
				var function = expression.Compile(input1);

				mockF1To1Compiler.Verify(
					o => o.GetFunction(It.IsAny<Input[]>(), It.IsAny<Expression[]>()),
					Times.Once());

				Assert.That(function, Is.SameAs(mockF1To1.Object));
			}
		}

		[TestFixture]
		public class Compile_Input2 : TestBase
		{
			internal Input input1;

			public override void SetUp()
			{
				base.SetUp();

				input1 = new Input("x");
			}

			// more of an end-to-end test
			[Test]
			public void ThrowsWhenMissingInput()
			{
				var input2 = new Input("y");
				var expression = 2*input1;

				Assert.That(
					() => expression.Compile(input2),
					Throws.Exception);
			}

			// more of an end-to-end test
			[Test]
			public void WorksForExtraInputs()
			{
				var input2 = new Input("y");
				var expression = 2*input1;

				Assert.That(
					() => expression.Compile(input1, input2),
					Throws.Nothing);
			}
		}

		[TestFixture]
		public class Compile_Inputs : TestBase
		{
			internal Input input1;
			internal Input input2;
			internal Mock<ICompiler<IFunctionManyTo1>> mockFNTo1Compiler;
			internal Mock<IFunctionManyTo1> mockFNTo1;

			public override void SetUp()
			{
				base.SetUp();

				input1 = new Input("x");
				input2 = new Input("y");
				mockFNTo1Compiler = new Mock<ICompiler<IFunctionManyTo1>>(MockBehavior.Strict);
				mockFNTo1 = new Mock<IFunctionManyTo1>(MockBehavior.Strict);

				CompilerFactory.SetFunctionManyTo1Compiler(mockFNTo1Compiler.Object);

				mockFNTo1Compiler
					.Setup(o => o.GetFunction(It.IsAny<Input[]>(), It.IsAny<Expression[]>()))
					.Returns(mockFNTo1.Object);
			}

			[Test]
			public void TestTestTest()
			{
				IFunctionManyTo1 function = expression.Compile(input1, input2);

				mockFNTo1Compiler.Verify(
					o => o.GetFunction(It.IsAny<Input[]>(), It.IsAny<Expression[]>()),
					Times.Once());
				Assert.That(function, Is.SameAs(mockFNTo1.Object));
			}
		}

		[TestFixture]
		public class Compile_Inputs2 : TestBase
		{
			internal Input input1;
			internal Input input2;

			public override void SetUp()
			{
				base.SetUp();

				input1 = new Input("x");
				input2 = new Input("y");
			}

			// more of an end-to-end test
			[Test]
			public void ThrowsWhenMissingInput()
			{
				var expression = 2 * input1 * input2;

				Assert.That(
					() => expression.Compile(input2),
					Throws.Exception);
			}

			[Test]
			public void ThrowsWhenMissingInputAndAnExtra()
			{
				var input3 = new Input("z");
				var expression = 2 * input1 * input2;

				Assert.That(
					() => expression.Compile(input2, input3),
					Throws.Exception);
			}

			// more of an end-to-end test
			[Test]
			public void WorksForExtraInputs()
			{
				var input3 = new Input("z");
				var expression = 2 * input1 * input2;

				Assert.That(
					() => expression.Compile(input1, input2, input3),
					Throws.Nothing);
			}
		}

		[TestFixture]
		public class GetDerivative : TestBase
		{
		}
		[TestFixture]
		public class GetDerivatives : TestBase
		{
		}

		[TestFixture]
		public class CompileDerivative : TestBase
		{
		}
		[TestFixture]
		public class CompileDerivatives : TestBase
		{
		}
	}
}
