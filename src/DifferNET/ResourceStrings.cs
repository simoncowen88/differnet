﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET
{
	internal static class ResourceStrings
	{
		internal const string ModelMustHaveExactlyOneInput
			= "The model must have exactly one input.";
		internal const string UnexpectedInput
			= "An unexpected input was encountered.";

		internal const string UnexpectedPrecedent
			= "An unexpected precedent was encountered.";
		internal const string PrecedentUnmentioned
			= "A precendent of this expressions was not mentioned.";
		internal const string DuplicatePrecedent
			= "A precendent of this expressions was mentioned more than once.";
		
		internal const string GetDerivativeExpressionReturnedNull
			= "CustomExpression.GetDerivativeExpression returned null.";
		internal const string GetLinqExpressionReturnedNull
			= "CustomExpression.GetLinqExpression returned null.";

		internal const string LinqExpressionWasNull
			= "Linq expression was null.";

		internal const string FunctionRequiresExactlyOneInput
			= "This function type requires exactly one input.";
		internal const string FunctionRequiresExactlyOneOutput
			= "This function type requires exactly one output.";

		internal const string InputsMustNotBeEmpty
			= "Inputs must not be empty.";
	}
}
