﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET
{
	internal interface IExpressionVisitor_GetDerivatives
		: IExpressionVisitor<IEnumerable<ExpressionDerivative>, Unit>
	{
	}

	internal sealed class ExpressionVisitor_GetDerivatives
		: IExpressionVisitor_GetDerivatives
	{
		public IEnumerable<ExpressionDerivative> Visit(
			Unit parameter,
			Input expression)
		{
			yield break;
		}

		private static ExpressionDerivative CreateDerivative(
			Expression precedent,
			Expression derivative)
		{
			return new ExpressionDerivative(precedent, derivative);
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Constant expression)
		{
			yield break;
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Abs expression)
		{
			yield return CreateDerivative(
				expression.X,
				Step.Of(expression.X, -1d, double.NaN, 1d));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Division expression)
		{
			yield return CreateDerivative(
				expression.Numerator,
				1d/expression.Denominator);

			yield return CreateDerivative(
				expression.Denominator,
				-expression.Numerator/(expression.Denominator*expression.Denominator));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Exp expression)
		{
			yield return CreateDerivative(
				expression.Exponent,
				expression);
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Ln expression)
		{
			yield return CreateDerivative(
				expression.X,
				1d/expression.X);
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Log expression)
		{
			yield return CreateDerivative(
				expression.X,
				1d/(expression.X*EMath.Log(expression.Base)));

			yield return CreateDerivative(
				expression.Base,
				-EMath.Log(expression.X)/
				(EMath.Log(expression.Base)*EMath.Log(expression.Base)*expression.Base))
				;
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Negate expression)
		{
			yield return CreateDerivative(
				expression.X,
				Constant.MinusOne);
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Power expression)
		{
			yield return CreateDerivative(
				expression.X,
				expression.Exponent*EMath.Pow(expression.X, expression.Exponent - 1d));

			yield return CreateDerivative(
				expression.Exponent,
				expression*EMath.Log(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Product expression)
		{
			for (int i = 0; i < expression.Precedents.Length; i++)
			{
				var index = i; // to avoid 'access to modified closure' R# warning
				var derivative = EMath.Product(expression.Precedents.Where((e, j) => j != index));

				yield return CreateDerivative(
					expression.Precedents[i],
					derivative);
			}
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, SimpleLog expression)
		{
			yield return CreateDerivative(
				expression.X,
				(1d/Math.Log(expression.Base))/expression.X);
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, SimplePower expression)
		{
			yield return CreateDerivative(
				expression.X,
				expression.Exponent*EMath.Pow(expression.X, expression.Exponent - 1d));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Sqrt expression)
		{
			yield return CreateDerivative(
				expression.X,
				0.5d/EMath.Sqrt(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Step expression)
		{
			// ---
			// ReSharper disable CompareOfFloatsByEqualityOperator
			if (expression.LeftValue == expression.MidValue &&
			    expression.MidValue == expression.RightValue)
				// ReSharper restore CompareOfFloatsByEqualityOperator
			{
				yield return CreateDerivative(
					expression.X,
					Constant.Zero);
				yield break;
			}

			// _-'
			// _--
			// __-
			// e.g. Heavyside
			if (expression.LeftValue <= expression.MidValue &&
			    expression.MidValue <= expression.RightValue)
			{
				yield return CreateDerivative(
					expression.X,
					Step.Of(expression.X, 0d, double.PositiveInfinity, 0d));
				yield break;
			}

			// '-_
			// --_
			// -__
			// e.g. -Heavyside
			if (expression.LeftValue >= expression.MidValue &&
			    expression.MidValue >= expression.RightValue)
			{
				yield return CreateDerivative(
					expression.X,
					Step.Of(expression.X, 0d, double.NegativeInfinity, 0d));
				yield break;
			}

			// -_-
			// _-_
			yield return CreateDerivative(
				expression.X,
				Step.Of(expression.X, 0d, double.NaN, 0d));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Sum expression)
		{
			return expression.Precedents.Select(x => CreateDerivative(x, Constant.One));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcCos expression)
		{
			// dy/dx arccos(x) = -1 / (sqrt(1 - x*x))

			yield return CreateDerivative(
				expression.X,
				-1d/EMath.Sqrt(1d - expression.X*expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcCosec expression)
		{
			// dy/dx arccosec(x) = arcsin(1/x) = -1 / (x*x * sqrt(1 - x^-2))
			// for real x: = -1 / (abs(x) * sqrt(x*x - 1)) for abs(x) > 1

			yield return CreateDerivative(
				expression.X,
				-1d/(expression.X*expression.X*EMath.Sqrt(1d - 1d/(expression.X*expression.X))));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcCot expression)
		{
			// dy/dx arccot(x) = arctan(1/x) = -1 / (1 + x*x)

			yield return CreateDerivative(
				expression.X,
				-1d/(1d + expression.X*expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcSec expression)
		{
			// dy/dx arcsec(x) = arccos(1/x) = 1 / (x*x * sqrt(1 - x^-2))
			// for real x: = 1 / (abs(x) * sqrt(x*x - 1)) for abs(x) > 1

			yield return CreateDerivative(
				expression.X,
				1d/(expression.X*expression.X*EMath.Sqrt(1d - 1d/(expression.X*expression.X))));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcSin expression)
		{
			// dy/dx arcsin(x) = 1 / (sqrt(1 - x*x))

			yield return CreateDerivative(
				expression.X,
				1d/EMath.Sqrt(1d - expression.X*expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcTan expression)
		{
			// dy/dx arctan(x) = 1 / (1 + x*x)

			yield return CreateDerivative(
				expression.X,
				1d/(1d + expression.X*expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, ArcTan2 expression)
		{
			// d/dy arctan(y/x) = 1 / sqrt(y*y + x*x)
			// d/dx arctan(y/x) = -y / (x * sqrt(y*y + x*x))

			yield return CreateDerivative(
				expression.Y,
				1d/EMath.Sqrt(expression.Y*expression.Y + expression.X*expression.X));

			yield return CreateDerivative(
				expression.X,
				-expression.Y/(expression.X*EMath.Sqrt(1d + expression.X*expression.X)));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Cos expression)
		{
			// dy/dx cos(x) = -sin(x)

			yield return CreateDerivative(
				expression.X,
				-EMath.Sin(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Cosec expression)
		{
			// dy/dx csc(x) = -csc(x) cot(x) = -1 / (sin(x)tan(x))

			yield return CreateDerivative(
				expression.X,
				1d/(EMath.Sin(expression.X)*EMath.Tan(expression.X)));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Cosh expression)
		{
			yield return CreateDerivative(
				expression.X,
				EMath.Sinh(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Cot expression)
		{
			// dy/dx cot(x) = -csc^2(x) = -1/sin^2(x)

			yield return CreateDerivative(
				expression.X,
				-1d/(EMath.Sin(expression.X)*EMath.Sin(expression.X)));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Sec expression)
		{
			// dy/dx sec(x) = sec(x) tan(x) = tan(x)/cos(x)

			yield return CreateDerivative(
				expression.X,
				EMath.Tan(expression.X)/EMath.Cos(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Sin expression)
		{
			// dy/dx sin(x) = cos(x)

			yield return CreateDerivative(
				expression.X,
				EMath.Cos(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Sinh expression)
		{
			yield return CreateDerivative(
				expression.X,
				EMath.Cosh(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Tan expression)
		{
			// dy/dx tan(x) = sec^2(x) = 1/cos^2(x)

			yield return CreateDerivative(
				expression.X,
				1d/(EMath.Cos(expression.X)*EMath.Cos(expression.X)));
			//yield return CreateDerivative(
			//	expression.X,
			//	EMath.Pow(EMath.Cos(expression.X), -2));
		}

		public IEnumerable<ExpressionDerivative> Visit(Unit parameter, Tanh expression)
		{
			// dy/dx = 1 - tanh^2(x) = 1/cosh^2(x)
			yield return CreateDerivative(
				expression.X,
				1d - EMath.Tanh(expression.X)*EMath.Tanh(expression.X));
		}

		public IEnumerable<ExpressionDerivative> Visit(
			Unit parameter,
			CustomExpression expression)
		{
			var derivatives = expression.GetDerivativeExpression();

			if (derivatives == null)
				throw new Exception(ResourceStrings.GetDerivativeExpressionReturnedNull);

			return derivatives;
		}
	}
}