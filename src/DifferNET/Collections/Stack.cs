﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET.Collections
{
	internal class StackX<T> : IQueueStack<T>, IReadOnlyCollection<T>
	{
		private readonly Stack<T> underlying;

		public StackX()
		{
			underlying = new Stack<T>();
		}

		public int Count
		{
			get { return underlying.Count; }
		}

		public void Push(T item)
		{
			underlying.Push(item);
		}

		public T Pop()
		{
			return underlying.Pop();
		}

		public T Peek()
		{
			return underlying.Peek();
		}

		public IEnumerator<T> GetEnumerator()
		{
			return underlying.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}

}
