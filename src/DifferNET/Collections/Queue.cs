﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET.Collections
{
	internal class QueueX<T> : IQueueStack<T>, IReadOnlyCollection<T>
	{
		private readonly Queue<T> underlying;

		public QueueX()
		{
			underlying = new Queue<T>();
		}

		public int Count
		{
			get { return underlying.Count; }
		}

		public void Push(T item)
		{
			underlying.Enqueue(item);
		}

		public T Pop()
		{
			return underlying.Dequeue();
		}

		public T Peek()
		{
			return underlying.Peek();
		}

		public IEnumerator<T> GetEnumerator()
		{
			return underlying.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
