﻿
using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET.Collections
{
	internal interface IQueueStack<T> : IEnumerable<T>
	{
		void Push(T item);
		T Pop();
		T Peek();
	}
}
