﻿using System;
using System.Collections.Generic;
using System.Linq;
using LX = System.Linq.Expressions;

namespace DifferNET
{
	internal static class ExtensionMethods_
	{
		internal static double ConstantValue(this Expression constantExpression)
		{
			return ((Constant)constantExpression).Value;
		}
	}

	internal static class Helpers
	{
		private static Expression[] CollapseConstants(
			Expression[] expressions,
			double aggregateSeed,
			Func<double, Constant, double> aggregateFunc)
		{
			// collapse constants together...
			var constants = expressions
				.OfType<Constant>()
				.ToList();
				//.Where(e => e is Constant).Cast<Constant>();

			if (!constants.Any())
				return expressions;

			double foldedConstant = constants.Aggregate(aggregateSeed, aggregateFunc);

			var nonConstants = expressions
				.Where(e => !(e is Constant));
			
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			// Acceptable as this is a special short-circuit case, when the constants aggregate to 'nothing'
			if (foldedConstant == aggregateSeed)
				return nonConstants.ToArray();

			return new[] { Constant.Of(foldedConstant) }
				.Concat(nonConstants)
				.ToArray();
		}

		internal static Expression[] CollapseSumConstants(Expression[] expressions)
		{
			return CollapseConstants(
				expressions,
				0d,
				(agg, c) => agg + c.Value);
		}

		internal static Expression[] CollapseProductConstants(Expression[] expressions)
		{
			return CollapseConstants(
				expressions,
				1d,
				(agg, c) => agg * c.Value);
		}

		internal static Expression[] ExpandSubExpressionsOfType<T>(Expression[] expressions)
			where T : Expression
		{
			var subExpressionsOfType = expressions
				.Where(e => e is T)
				.ToList();
			//.Cast<T>();

			if (!subExpressionsOfType.Any())
				return expressions;

			var expandedSubExpressions = subExpressionsOfType
				.SelectMany(e => e.Precedents);

			var otherSubExpressions = expressions
				.Where(e => !(e is T));

			return otherSubExpressions
				.Concat(expandedSubExpressions)
				.ToArray();
		}

		// 
		//internal static Expression[] CollapseSumRepeats(Expression[] expressions)
		//{
		//	var expressionRepeats = new Dictionary<Expression, int>();
			
		//	foreach (var expression in expressions)
		//	{
		//		if (!expressionRepeats.ContainsKey(expression))
		//			expressionRepeats.Add(expression, 0);

		//		expressionRepeats[expression]++;
		//	}

		//	var nonRepeats = expressionRepeats
		//		.Where(kvp => kvp.Value == 1)
		//		.Select(kvp => kvp.Key);
		//	var repeats = expressionRepeats
		//		.Where(kvp => kvp.Value != 1)
		//		.Select(kvp => kvp.Value * kvp.Key);

		//	return nonRepeats.Concat(repeats).ToArray();
		//}
	}
}
