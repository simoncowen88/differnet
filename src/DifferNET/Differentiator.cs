﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LX = System.Linq.Expressions;

namespace DifferNET
{
	internal interface IDifferentiator
	{
		IMap<Input, Expression> CreateDerivatives(
			IReadOnlyCollection<Input> inputs, 
			Expression output);
	}

	internal class Differentiator : IDifferentiator
	{
		#region Fields

		private readonly IExpressionVisitor_GetDerivatives derivativesVisitor;

		#endregion

		#region Constructors

		public Differentiator(
			IExpressionVisitor_GetDerivatives derivativesVisitor)
		{
			this.derivativesVisitor = derivativesVisitor;
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		public IMap<Input, Expression> CreateDerivatives(
			IReadOnlyCollection<Input> inputs,
			Expression output)
		{
			if (inputs == null || inputs.Count == 0)
				throw new ArgumentException(ResourceStrings.InputsMustNotBeEmpty);

			var expressions = ExpressionTraverser.PrecendentsLast(output).Expressions;

			// TODO: We could... (does this make things clearer???)
			// Change map to have values of simply Expression (the 'partial derivative')
			// Remove the ExpressionDual class
			// 
			// dualMap = dualMap.With(
			//	derivative.Precedent, 
			//	dualMap[derivative.Precedent] + dualMap[expression]*expressionDerivative.Derivative);

			// create all of the (empty) duals
			var dualMap = GetDualMap(expressions, output);

			// accumulate derivatives backwards from the output
			foreach (var expression in expressions)
			{
				var dual = dualMap[expression];
				dual.PushDerivativesToPrecedents(derivativesVisitor, dualMap);
			}

			// pick out the derivatives for the inputs
			var derivatives = GetDerivativesForInputs(inputs, dualMap);
			return derivatives;
		}

		private static Map<Expression, ExpressionDual> GetDualMap(
			IReadOnlyCollection<Expression> expressions,
			Expression output)
		{
			// output will be the FIRST item in expressions
			Debug.Assert(ReferenceEquals(output, expressions.First()));

			var dualMap = new Map<Expression, ExpressionDual>();

			// we must 'seed' the root dual with 1d
			var outputDual = new ExpressionDual(output, Constant.One);
			dualMap = dualMap.With(output, outputDual);

			// now we skip the output
			foreach (var expression in expressions.Skip(1))
			{
				var dual = new ExpressionDual(expression);
				dualMap = dualMap.With(expression, dual);
			}

			return dualMap;
		}

		private Map<Input, Expression> GetDerivativesForInputs(
			IEnumerable<Input> inputs,
			Map<Expression, ExpressionDual> dualMap)
		{
			var derivatives = new Map<Input, Expression>();

			foreach (var input in inputs)
			{
				// get the derivative wrt to this input
				var derivative = GetDerivativeForInput(input, dualMap);
				derivatives = derivatives.With(input, derivative);
			}

			return derivatives;
		}

		private static Expression GetDerivativeForInput(
			Input input,
			Map<Expression, ExpressionDual> dualMap)
		{
			ExpressionDual dual;
			if (dualMap.TryGetValue(input, out dual))
			{
				return dual.Derivative;
			}

			// if we never saw the input at all...
			return Constant.Zero;
		}

		#endregion
	}
}
