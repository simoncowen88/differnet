﻿using System;
using System.Collections.Generic;
using System.Linq;
using DifferNET.Annotations;
using DifferNET.Compiled;
using LX = System.Linq.Expressions;

namespace DifferNET
{
	/// <summary>
	/// Represents a set of inputs and provides convenient methods for compiling
	/// and differentiating expressions created from these inputs.
	/// </summary>
	public sealed class Model
	{
		#region Fields

		private readonly List<Input> inputs;
		private int inputCounter;

		#endregion

		#region Contructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Model"/> class.
		/// </summary>
		public Model()
		{
			inputs = new List<Input>();
			inputCounter = 0;
		}

		#endregion

		#region Properties
		#endregion

		#region Methods

		private Input GetSingleInput()
		{
			try
			{
				return inputs.Single();
			}
			catch (Exception exception)
			{
				throw new InvalidOperationException(
					ResourceStrings.ModelMustHaveExactlyOneInput,
					exception);
			}
		}

		#endregion

		#region Public methods

		/// <summary>Creates a new input for the model.</summary>
		/// <returns>
		/// The newly created <see cref="Input" />.
		/// </returns>
		[PublicAPI]
		public Input CreateInput()
		{
			var inputName = string.Format("x_{0}", inputCounter);
			return CreateInput(inputName);
		}

		/// <summary>Creates a new input for the model.</summary>
		/// <param name="inputName">The name of the input to create.</param>
		/// <returns>
		/// The newly created <see cref="Input" />.
		/// </returns>
		[PublicAPI]
		public Input CreateInput(string inputName)
		{
			var input = new Input(inputName);

			inputs.Add(input);
			inputCounter++;

			return input;
		}

		/// <summary>
		/// Creates a compiled function for the specified expression.
		/// The model must have exactly one input for this to be valid.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>
		/// A compiled function for the specified expression.
		/// </returns>
		[PublicAPI]
		public IFunction1To1 CompileForSingleInput(Expression expression)
		{
			var input = GetSingleInput();
			return expression.Compile(input);
		}

		/// <summary>
		/// Creates a compiled function for the specified expressions.
		/// The model must have exactly one input for this to be valid.
		/// </summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns>
		/// A compiled function for the specified expressions, using the ordering of the
		/// expressions for the ordering of arguments to the function.
		/// </returns>
		[PublicAPI]
		public IFunction1ToMany CompileForSingleInput(params Expression[] expressions)
		{
			var input = GetSingleInput();
			return expressions.Compile(input);
		}

		/// <summary>
		/// Creates a compiled function for the specified expression.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>
		/// A compiled function for the specified expression, using the order of 
		/// creation of inputs for the ordering of arguments to the function.
		/// </returns>
		[PublicAPI]
		public IFunctionManyTo1 Compile(Expression expression)
		{
			return expression.Compile(inputs.ToArray());
		}

		/// <summary>
		/// Creates a compiled function for the specified expressions.
		/// </summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns>
		/// A compiled function for the specified expression, using the order of 
		/// creation of inputs and the ordering of the expressions for the ordering 
		/// of arguments to the function.
		/// </returns>
		[PublicAPI]
		public IFunctionManyToMany Compile(params Expression[] expressions)
		{
			return expressions.Compile(inputs.ToArray());
		}

		/// <summary>
		/// Gets the derivatives for the specified expression, returning a map
		/// from each input to the derivative with respect that input.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>The derivatives for the specified expression.</returns>
		[PublicAPI]
		public IMap<Input, Expression> GetDerivatives(Expression expression)
		{
			return expression.GetDerivatives(inputs.ToArray());
		}

		/// <summary>
		/// Creates a compiled function for the derivatives of the specified expression.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>
		/// A compiled function for the derivatives of the specified expression.
		/// </returns>
		[PublicAPI]
		public IFunctionManyToMany CompileDerivatives(Expression expression)
		{
			return expression.CompileDerivatives(inputs.ToArray());
		}

		/// <summary>
		/// Creates a compiled function for the derivative of the specified expression.
		/// The model must have exactly one input for this to be valid.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <returns>
		/// A compiled function for the derivative of the specified expression.
		/// </returns>
		[PublicAPI]
		public IFunction1To1 CompileDerivativeForSingleInput(Expression expression)
		{
			var input = GetSingleInput();
			return expression.CompileDerivative(input);
		}

		/// <summary>
		/// Creates a compiled function for the derivative of the specified expressions.
		/// The model must have exactly one input for this to be valid.
		/// </summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns>
		/// A compiled function for the derivatives of the specified expressions.
		/// </returns>
		[PublicAPI]
		public IFunction1ToMany CompileDerivativesForSingleInput(params Expression[] expressions)
		{
			var input = GetSingleInput();
			return expressions.CompileDerivatives(input);
		}

		#endregion
	}
}
