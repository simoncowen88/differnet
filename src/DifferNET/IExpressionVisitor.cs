﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET
{
	internal interface IExpressionVisitor<out TReturn, in TParameter>
	{
		// ReSharper disable once UnusedParameter.Global // for consistency
		TReturn Visit(TParameter parameter, Input expression);
		// ReSharper disable once UnusedParameter.Global // for consistency
		TReturn Visit(TParameter parameter, Constant expression);

		TReturn Visit(TParameter parameter, Abs expression);
		TReturn Visit(TParameter parameter, Division expression);
		TReturn Visit(TParameter parameter, Exp expression);
		TReturn Visit(TParameter parameter, Ln expression);
		TReturn Visit(TParameter parameter, Log expression);
		TReturn Visit(TParameter parameter, Negate expression);
		TReturn Visit(TParameter parameter, Power expression);
		TReturn Visit(TParameter parameter, Product expression);
		TReturn Visit(TParameter parameter, SimpleLog expression);
		TReturn Visit(TParameter parameter, SimplePower expression);
		TReturn Visit(TParameter parameter, Sqrt expression);
		TReturn Visit(TParameter parameter, Step expression);
		TReturn Visit(TParameter parameter, Sum expression);

		TReturn Visit(TParameter parameter, ArcCos expression);
		TReturn Visit(TParameter parameter, ArcCosec expression);
		TReturn Visit(TParameter parameter, ArcCot expression);
		TReturn Visit(TParameter parameter, ArcSec expression);
		TReturn Visit(TParameter parameter, ArcSin expression);
		TReturn Visit(TParameter parameter, ArcTan expression);
		TReturn Visit(TParameter parameter, ArcTan2 expression);
		TReturn Visit(TParameter parameter, Cos expression);
		TReturn Visit(TParameter parameter, Cosec expression);
		TReturn Visit(TParameter parameter, Cosh expression);
		TReturn Visit(TParameter parameter, Cot expression);
		TReturn Visit(TParameter parameter, Sec expression);
		TReturn Visit(TParameter parameter, Sin expression);
		TReturn Visit(TParameter parameter, Sinh expression);
		TReturn Visit(TParameter parameter, Tan expression);
		TReturn Visit(TParameter parameter, Tanh expression);
		TReturn Visit(TParameter parameter, CustomExpression expression);
	}
}
