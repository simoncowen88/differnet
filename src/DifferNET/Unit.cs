﻿namespace DifferNET
{
	internal struct Unit
	{
		public static readonly Unit Default = new Unit();

		public override int GetHashCode()
		{
			return 0;
		}

		public override bool Equals(object obj)
		{
			return obj is Unit;
		}

		public override string ToString()
		{
			return "[unit]";
		}
	}
}