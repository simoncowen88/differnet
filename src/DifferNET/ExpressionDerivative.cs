﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET
{
	/// <summary>
	/// Represents the derivative of an expression with respect to one of its precedents.
	/// </summary>
	public sealed class ExpressionDerivative
	{
		#region Fields

		private readonly Expression precedent;
		private readonly Expression derivative;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ExpressionDerivative" /> class.
		/// </summary>
		/// <param name="precedent">The precedent of the expression.</param>
		/// <param name="derivative">The derivative of the expression with respect to the precedent.</param>
		public ExpressionDerivative(Expression precedent, Expression derivative)
		{
			this.precedent = precedent;
			this.derivative = derivative;
		}

		#endregion

		#region Properties

		/// <summary>Gets the precedent of the expression.</summary>
		/// <value>The precedent of the expression.</value>
		public Expression Precedent
		{
			get { return precedent; }
		}

		/// <summary>
		/// Gets the derivative of the expression with respect to the precedent.
		/// </summary>
		/// <value>
		/// The derivative of the expression with respect to the precedent.
		/// </value>
		public Expression Derivative
		{
			get { return derivative; }
		}

		#endregion

		#region Methods
		#endregion
	}
}
