﻿using System;
using System.Collections.Generic;
using System.Linq;
using DifferNET.Compiled;

namespace DifferNET
{
	/// <summary>
	/// Contains extensions methods for working with Expressions.
	/// </summary>
	public static class ExpressionExtensions
	{
		/// <summary>
		/// Creates a compiled function for the specified expressions.
		/// </summary>
		/// <param name="expressions">The expressions.</param>
		/// <param name="inputs">The inputs for the expressions.</param>
		/// <returns>
		/// A compiled function for the expressions, using the ordering of the
		/// expressions and specified inputs for the ordering of arguments to the function.
		/// </returns>
		public static IFunctionManyToMany Compile(
			this IEnumerable<Expression> expressions,
			params Input[] inputs)
		{
			var compiler = CompilerFactory.FunctionManyToManyCompiler;
			return compiler.GetFunction(inputs, expressions.ToArray());
		}

		/// <summary>Creates a compiled function for the expressions.</summary>
		/// <param name="expressions">The expressions.</param>
		/// <param name="input">The input for the expressions.</param>
		/// <returns>
		/// A compiled function for the expressions, using the ordering of the
		/// expressions for the ordering of arguments to the function.
		/// </returns>
		public static IFunction1ToMany Compile(
			this IEnumerable<Expression> expressions,
			Input input)
		{
			var compiler = CompilerFactory.Function1ToManyCompiler;
			return compiler.GetFunction(new[] {input}, expressions.ToArray());
		}

		/// <summary>
		/// Creates a compiled function for the derivatives of the specified expressions.
		/// </summary>
		/// <param name="expressions">The expressions.</param>
		/// <param name="input">The input to the expressions.</param>
		/// <returns>
		/// A compiled function for the derivatives of the expressions.
		/// </returns>
		public static IFunction1ToMany CompileDerivatives(
			this IEnumerable<Expression> expressions,
			Input input)
		{
			var derivatives = expressions
				.Select(e => e.GetDerivative(input));

			return derivatives.Compile(input);
		}
	}
}
