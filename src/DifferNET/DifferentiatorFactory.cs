﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DifferNET
{
	internal static class DifferentiatorFactory
	{
		#region Fields

		private static IDifferentiator differentiator;

		#endregion

		#region Constructors

		static DifferentiatorFactory()
		{
			SetDifferentiatorToDefault();
		}

		#endregion

		#region Methods

		internal static IDifferentiator GetDifferentiator()
		{
			return differentiator;
		}

		internal static void SetDifferentiatorToDefault()
		{
			differentiator = new Differentiator(new ExpressionVisitor_GetDerivatives());
		}

		#endregion

		#region Set methods for testing

		// FOR TESTING ONLY.

		[Conditional("DEBUG")]
		internal static void SetDifferentiator(IDifferentiator _differentiator)
		{
			differentiator = _differentiator;
		}

		#endregion
	}
}
