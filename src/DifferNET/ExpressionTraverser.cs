﻿using DifferNET.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET
{
	internal class ExpressionTraverser
	{
		#region Fields

		private readonly IReadOnlyCollection<Expression> expressions;
		private readonly IReadOnlyCollection<Expression> expressionsPrecedentsFirst;
		private readonly IReadOnlyCollection<Expression> expressionsPrecedentsLast;

		// probably make these lazy...
		private readonly Dictionary<Expression, int> minDistanceFromInput;
		private readonly Dictionary<Expression, int> directReuseCount;
		private readonly Dictionary<Expression, int> subTreeSize;

		#endregion

		#region Constructors

		private ExpressionTraverser(
			IReadOnlyCollection<Expression> expressions,
			IReadOnlyCollection<Expression> expressionsPrecedentsFirst,
			IReadOnlyCollection<Expression> expressionsPrecedentsLast)
		{
			this.expressions = expressions;
			this.expressionsPrecedentsFirst = expressionsPrecedentsFirst;
			this.expressionsPrecedentsLast = expressionsPrecedentsLast;

			minDistanceFromInput = GetMinDistanceFromInput();
			directReuseCount = GetDirectReuseCount();
			subTreeSize = GetSubtreeSize();
		}

		#endregion

		#region Properties

		internal IReadOnlyCollection<Expression> Expressions
		{
			get { return expressions; }
		}

		#endregion

		#region Methods

		// This is the minimum distance to an input
		internal int MinDistanceFromInput(Expression expression)
		{
			return minDistanceFromInput[expression];
		}
		internal int DirectReuseCount(Expression expression)
		{
			return directReuseCount[expression];
		}
		internal int SubTreeSize(Expression expression)
		{
			return subTreeSize[expression];
		}

		private Dictionary<Expression, int> GetMinDistanceFromInput()
		{
			// this is "how many steps is it from this expression to an input?"
			var _minDistanceFromInput = new Dictionary<Expression, int>();

			foreach (var expression in expressionsPrecedentsFirst)
			{
				int initialDistance = (expression is Constant) ? 1000000 : 0;
				_minDistanceFromInput.Add(expression, initialDistance);
			}
			foreach (var expression in expressionsPrecedentsFirst) // inputs -> outputs
			{
				if (!expression.Precedents.Any()) continue;

				var precedentsMin = expression.Precedents.Min(p => _minDistanceFromInput[p]);
				_minDistanceFromInput[expression] = precedentsMin + 1;
			}

			return _minDistanceFromInput;
		}

		private Dictionary<Expression, int> GetDirectReuseCount()
		{
			// this is (or should be) "how many times is this expression an input to another expression?"
			var _directReuseCount = new Dictionary<Expression, int>();

			foreach (var expression in expressionsPrecedentsFirst)
			{
				_directReuseCount.Add(expression, 0);
			}

			foreach (var expression in expressionsPrecedentsFirst) // inputs -> outputs
			{
				foreach (var precedent in expression.Precedents)
				{
					_directReuseCount[precedent]++;
				}
			}

			return _directReuseCount;
		}

		private Dictionary<Expression, int> GetSubtreeSize()
		{
			// this is "how many expressions are below this expression?"
			var _subTreeSize = new Dictionary<Expression, int>();

			foreach (var expression in expressionsPrecedentsFirst)
			{
				_subTreeSize.Add(expression, expression.Precedents.Length);
			}

			foreach (var expression in expressionsPrecedentsFirst) // inputs -> outputs
			{
				foreach (var precendent in expression.Precedents)
				{
					_subTreeSize[expression] += _subTreeSize[precendent];
				}
			}

			return _subTreeSize;
		}

		#endregion

		#region Static

		// aka OutputsFirst
		internal static ExpressionTraverser PrecendentsLast(params Expression[] outputs)
		{
			var expressionsPrecedentsFirst = _PrecendentsFirst(outputs);
			var expressionsPrecedentsLast = _PrecendentsLast(outputs);

			return new ExpressionTraverser(
				expressionsPrecedentsLast,
				expressionsPrecedentsFirst,
				expressionsPrecedentsLast);
		}

		// aka OutputsLast
		internal static ExpressionTraverser PrecendentsFirst(params Expression[] outputs)
		{
			var expressionsPrecedentsFirst = _PrecendentsFirst(outputs);
			var expressionsPrecedentsLast = _PrecendentsLast(outputs);

			return new ExpressionTraverser(
				expressionsPrecedentsFirst,
				expressionsPrecedentsFirst,
				expressionsPrecedentsLast);
		}

		private static IReadOnlyCollection<Expression> _PrecendentsLast(params Expression[] outputs)
		{
			var expressions = FindAllExpressions(outputs);

			var uniqueExpressions = new StackX<Expression>();
			DeduplicateExpressions(expressions, uniqueExpressions);

			return uniqueExpressions;
		}

		private static IReadOnlyCollection<Expression> _PrecendentsFirst(params Expression[] outputs)
		{
			var expressions = FindAllExpressions(outputs);

			var uniqueExpressions = new QueueX<Expression>();
			DeduplicateExpressions(expressions, uniqueExpressions);

			return uniqueExpressions;
		}

		// Gets a stack of the expression discovered from the given outputs.
		// The outputs will be first on the stack. i.e. will pop LAST.
		private static Stack<Expression> FindAllExpressions(Expression[] outputs)
		{
			// The (as yet) unvisited nodes. Queue => breadth first.
			var unvisited = new Queue<Expression>(outputs);
			
			// The already visited nodes
			var visited = new Stack<Expression>();

			while (unvisited.Count > 0)
			{
				Expression current = unvisited.Dequeue();

				foreach (var precendent in current.Precedents)
				{
					unvisited.Enqueue(precendent);
				}

				visited.Push(current);
			}

			return visited;
		}

		private static void DeduplicateExpressions(
			IEnumerable<Expression> expressionsWithOutputsLast,
			IQueueStack<Expression> dedupedExpressions)
		{
			// We now de-duplicate the list of expressions.
			// We must leave the FIRST occurence of an expression alone! 
			// So that we do not break the 
			// "each expression only relies on expression before it in the list" 
			// property.

			var visitedSet = new HashSet<Expression>();
			foreach (var expression in expressionsWithOutputsLast)
			{
				if (visitedSet.Add(expression))
				{
					dedupedExpressions.Push(expression);
				}
			}
		}

		#endregion
	}
}
