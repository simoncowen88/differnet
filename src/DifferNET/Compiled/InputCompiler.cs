﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	internal interface IInputCompiler
	{
		Type InputType { get; }

		IReadOnlyDictionary<Input, System.Linq.Expressions.Expression> GetInputMap(
			Input[] inputs,
			ParameterExpression inputExpression);
	}

	internal class SingleInputCompiler : IInputCompiler
	{
		public Type InputType
		{
			get { return typeof (double); }
		}

		public IReadOnlyDictionary<Input, System.Linq.Expressions.Expression> GetInputMap(
			Input[] inputs,
			ParameterExpression inputExpression)
		{
			var input = inputs.Single();
			var map = new Dictionary<Input, System.Linq.Expressions.Expression>();
			map.Add(input, inputExpression);
			return new ReadOnlyDictionary<Input, System.Linq.Expressions.Expression>(map);
		}
	}

	internal class ManyInputCompiler : IInputCompiler
	{
		public Type InputType
		{
			get { return typeof (double[]); }
		}

		public IReadOnlyDictionary<Input, System.Linq.Expressions.Expression> GetInputMap(
			Input[] inputs,
			ParameterExpression inputExpression)
		{
			var map = new Dictionary<Input, System.Linq.Expressions.Expression>();
			for (int i = 0; i < inputs.Length; i++)
			{
				var input = inputs[i];
				var arrayIndex = LinqHelpers.ArrayIndex(inputExpression, i);
				map.Add(input, arrayIndex);
			}
			return new ReadOnlyDictionary<Input, System.Linq.Expressions.Expression>(map);
		}
	}
}
