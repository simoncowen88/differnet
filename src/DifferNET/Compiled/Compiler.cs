namespace DifferNET.Compiled
{
	internal static class Compiler
	{
		internal static Compiler<TFunction, TDelegate> Create<TFunction, TDelegate>(
			IInputCompiler inputCompiler,
			IOutputCompiler outputCompiler,
			IFunctionCompiler<TFunction, TDelegate> functionCompiler)
		{
			return new Compiler<TFunction, TDelegate>(
				inputCompiler,
				outputCompiler,
				functionCompiler);
		}
	}
}