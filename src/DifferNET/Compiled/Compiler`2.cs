﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	internal class Compiler<TFunction, TDelegate> : ICompiler<TFunction>
	{
		#region Fields

		private readonly IExpressionVisitor_GetLinqExpression linqExpressionVisitor;
		private readonly IInputCompiler inputCompiler;
		private readonly IOutputCompiler outputCompiler;
		private readonly IFunctionCompiler<TFunction, TDelegate> functionCompiler;

		#endregion

		#region Constructors

		internal Compiler(
			IInputCompiler inputCompiler,
			IOutputCompiler outputCompiler,
			IFunctionCompiler<TFunction, TDelegate> functionCompiler)
		{
			linqExpressionVisitor = new ExpressionVisitor_GetLinqExpression();

			this.inputCompiler = inputCompiler;
			this.outputCompiler = outputCompiler;
			this.functionCompiler = functionCompiler;
		}

		#endregion

		#region Properties

		#endregion

		#region Methods
		
		public TFunction GetFunction(
			Input[] inputs,
			Expression[] outputs)
		{
			var inputExpression = GetInputExpression();
			var outputExpression = GetOutputExpression();

			var expression = GetExpression(
				inputs,
				inputExpression,
				outputs,
				outputExpression);

			var lambda = System.Linq.Expressions.Expression.Lambda<TDelegate>(
				expression,
				inputExpression,
				outputExpression);

			return functionCompiler.CreateFunction(
				lambda,
				inputs.Length,
				outputs.Length);
		}

		private ParameterExpression GetOutputExpression()
		{
			return System.Linq.Expressions.Expression.Parameter(
				outputCompiler.OutputType,
				"output");
		}

		private ParameterExpression GetInputExpression()
		{
			return System.Linq.Expressions.Expression.Parameter(
				inputCompiler.InputType,
				"x");
		}

		private System.Linq.Expressions.Expression GetExpression(
			Input[] inputs,
			ParameterExpression inputExpression,
			Expression[] outputs,
			ParameterExpression outputExpression)
		{
			// inputs must be handled specialy first
			var inputMap = inputCompiler.GetInputMap(
				inputs,
				inputExpression);

			var mainBlock = CreateMainBlock(
				inputs,
				outputs,
				inputMap);

			var outputExpressions = outputCompiler.GetOutputExpressions(
				outputs,
				outputExpression,
				mainBlock.ValueMap);

			return System.Linq.Expressions.Expression.Block(
				mainBlock.Variables,
				mainBlock.Expressions.Concat(outputExpressions));
		}

		private ExpressionBlock CreateMainBlock(
			Input[] inputs,
			Expression[] outputs,
			IReadOnlyDictionary<Input, System.Linq.Expressions.Expression> inputMap)
		{
			var expectedInputs = new HashSet<Input>(inputs);

			var valueMap = new Map<Expression, System.Linq.Expressions.Expression>();
			valueMap = AddInputValues(valueMap, inputMap);

			var expressionTraverser = ExpressionTraverser.PrecendentsFirst(outputs);
			// outputs are the LAST items in this array
			var expressions = expressionTraverser.Expressions;

			var blockVariables = new List<ParameterExpression>();
			var blockExpressions = new List<System.Linq.Expressions.Expression>();

			foreach (var expression in expressions)
			{
				// inputs are already in the map as they are handled specially
				if (IsValidInput(expression, expectedInputs))
					continue;

				var expressionValue = GetExpressionValue(
					expressionTraverser,
					expression,
					valueMap);
				
				if (expressionValue.Variable != null)
				{
					blockVariables.Add(expressionValue.Variable);
					blockExpressions.Add(expressionValue.VariableAssignment);
				}

				valueMap = valueMap.With(expression, expressionValue.Value);
			}

			return new ExpressionBlock(
				blockVariables,
				blockExpressions,
				valueMap);
		}

		private ExpressionValue GetExpressionValue(
			ExpressionTraverser expressionTraverser,
			Expression expression,
			IMap<Expression, System.Linq.Expressions.Expression> valueMap)
		{
			var useVariable = ShouldUseVariable(expressionTraverser, expression);

			var value = expression.Accept(linqExpressionVisitor, valueMap);

			if (!useVariable)
			{
				return new ExpressionValue(value);
			}
			
			var variable = System.Linq.Expressions.Expression.Variable(typeof (double));
			var assignExpression = System.Linq.Expressions.Expression.Assign(
				variable,
				value);

			return new ExpressionValue(variable, assignExpression);
		}

		private static bool IsValidInput(Expression expression, HashSet<Input> expectedInputs)
		{
			var input = expression as Input;

			// if it's not an input
			if (input == null)
				return false;

			// if it's an input we're expecting
			if (expectedInputs.Contains(input)) 
				return true;

			// if it's an input we're not expecting
			throw new Exception(ResourceStrings.UnexpectedInput);
		}

		private static Map<Expression, System.Linq.Expressions.Expression> AddInputValues(
			Map<Expression, System.Linq.Expressions.Expression> valueMap,
			IReadOnlyDictionary<Input, System.Linq.Expressions.Expression> inputMap)
		{
			// include inputs in map
			foreach (var kvp in inputMap)
			{
				valueMap = valueMap.With(kvp.Key, kvp.Value);
			}

			return valueMap;
		}

		private static bool ShouldUseVariable(
			ExpressionTraverser expressionTraverser,
			Expression expression)
		{
			if (expression is Constant) 
				return false;

			// TODO: ShouldUseVariable needs more work
			// Decide if reuse (using a variable) is valuable enough to warrant it.
			// Not efficient if a large subtree is reused many times...

			// Also, never let the call stack get too large.
			// NEVER using variables can lead to a StackOverflow....
			// e.g. for (int i = 0; i < 1000; i++) { y = Model.Sin(y); }

			int subTreeSize = expressionTraverser.SubTreeSize(expression);
			int directReuseCount = expressionTraverser.DirectReuseCount(expression);
			int minDistanceFromInput = expressionTraverser.MinDistanceFromInput(expression);

			bool useVariable = false;

			// we don't want large subtrees to be resused too many times
			if (directReuseCount > 1)
			{
				// boundaries: SubTreeSize | DirectReuseCount
				// 2 | 10
				// 4 | 5
				// 10 | 2
				useVariable |= subTreeSize * (directReuseCount - 1) > 20;
			}

			// A bit of a hack: this makes sure the stack doesn't get too big...
			useVariable |= minDistanceFromInput % 20 == 0;

			return useVariable;
		}

		#endregion

		#region Nested

		private class ExpressionValue
		{
			private readonly System.Linq.Expressions.Expression value;
			private readonly ParameterExpression variable;
			private readonly System.Linq.Expressions.Expression variableAssignment;

			public ExpressionValue(
				ParameterExpression variable,
				System.Linq.Expressions.Expression variableAssignment)
			{
				if (variable == null)
					throw new ArgumentNullException();
				if (variableAssignment == null)
					throw new ArgumentNullException();

				this.variable = variable;
				this.variableAssignment = variableAssignment;
				this.value = variable;
			}

			public ExpressionValue(System.Linq.Expressions.Expression value)
			{
				if (value == null)
					throw new Exception(ResourceStrings.LinqExpressionWasNull);

				this.value = value;
			}

			public System.Linq.Expressions.Expression Value
			{
				get { return value; }
			}

			// may be null
			public ParameterExpression Variable
			{
				get { return variable; }
			}

			// may be null
			public System.Linq.Expressions.Expression VariableAssignment
			{
				get { return variableAssignment; }
			}
		}

		private class ExpressionBlock
		{
			private readonly IReadOnlyCollection<ParameterExpression> variables;
			private readonly IReadOnlyCollection<System.Linq.Expressions.Expression> expressions;
			private readonly IMap<Expression, System.Linq.Expressions.Expression> valueMap;

			public ExpressionBlock(
				IEnumerable<ParameterExpression> variables,
				IEnumerable<System.Linq.Expressions.Expression> expressions,
				IMap<Expression, System.Linq.Expressions.Expression> valueMap)
			{
				this.variables = variables.ToList().AsReadOnly();
				this.expressions = expressions.ToList().AsReadOnly();
				// NOTE: this should really copy to be certain of read-only
				this.valueMap = valueMap;
			}

			public IReadOnlyCollection<ParameterExpression> Variables
			{
				get { return variables; }
			}

			public IReadOnlyCollection<System.Linq.Expressions.Expression> Expressions
			{
				get { return expressions; }
			}

			public IMap<Expression, System.Linq.Expressions.Expression> ValueMap
			{
				get { return valueMap; }
			}
		}

		#endregion
	}
}
