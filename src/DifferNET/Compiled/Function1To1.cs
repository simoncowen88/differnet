﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	/// <summary>
	/// Represents a function with a single input and a single output.
	/// </summary>
	public interface IFunction1To1
	{
		/// <summary>
		/// Invokes the compiled expression with the specified input, assigning
		/// the result to the specified output.
		/// </summary>
		/// <param name="input">The input to the function.</param>
		/// <param name="output">A variable to be assigned the result of the function.</param>
		void Invoke(double input, out double output);

		/// <summary>
		/// Invokes the compiled expression with the specified input.
		/// </summary>
		/// <param name="input">The input to the function.</param>
		/// <returns>The result of the function.</returns>
		double Invoke(double input);
	}

	internal partial class Function1To1 : IFunction1To1
	{
		internal delegate void FunctionDelegate(double input, out double output);

		#region Fields

		private readonly FunctionDelegate invokeFunc;

		#endregion

		#region Constructors

		internal Function1To1(
			Expression<FunctionDelegate> valueExpression)
		{
			invokeFunc = valueExpression.Compile();
		}

		#endregion

		#region Properties
		#endregion

		#region Methods
		
		public double Invoke(double input)
		{
			double output;
			invokeFunc(input, out output);
			return output;
		}

		public void Invoke(double input, out double output)
		{
			invokeFunc(input, out output);
		}

		#endregion
	}
}
