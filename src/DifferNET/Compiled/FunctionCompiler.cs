﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	internal interface IFunctionCompiler<out TFunction, TDelegate>
	{
		TFunction CreateFunction(
			Expression<TDelegate> lambda,
			int inputCount,
			int outputCount);
	}

	internal class Function1To1Compiler :
		IFunctionCompiler<IFunction1To1, Function1To1.FunctionDelegate>
	{
		public IFunction1To1 CreateFunction(
			Expression<Function1To1.FunctionDelegate> lambda,
			int inputCount,
			int outputCount)
		{
			if (inputCount != 1)
				throw new ArgumentOutOfRangeException(ResourceStrings.FunctionRequiresExactlyOneInput);
			if (outputCount != 1)
				throw new ArgumentOutOfRangeException(ResourceStrings.FunctionRequiresExactlyOneOutput);

			return new Function1To1(lambda);
		}
	}

	internal class Function1ToManyCompiler :
		IFunctionCompiler<IFunction1ToMany, Function1ToMany.FunctionDelegate>
	{
		public IFunction1ToMany CreateFunction(
			Expression<Function1ToMany.FunctionDelegate> lambda,
			int inputCount,
			int outputCount)
		{
			if (inputCount != 1)
				throw new ArgumentOutOfRangeException(ResourceStrings.FunctionRequiresExactlyOneInput);

			return new Function1ToMany(outputCount, lambda);
		}
	}

	internal class FunctionManyTo1Compiler :
		IFunctionCompiler<IFunctionManyTo1, FunctionManyTo1.FunctionDelegate>
	{
		public IFunctionManyTo1 CreateFunction(
			Expression<FunctionManyTo1.FunctionDelegate> lambda,
			int inputCount,
			int outputCount)
		{
			if (outputCount != 1)
				throw new ArgumentOutOfRangeException(ResourceStrings.FunctionRequiresExactlyOneOutput);

			return new FunctionManyTo1(inputCount, lambda);
		}
	}

	internal class FunctionManyToManyCompiler :
		IFunctionCompiler<IFunctionManyToMany, FunctionManyToMany.FunctionDelegate>
	{
		public IFunctionManyToMany CreateFunction(
			Expression<FunctionManyToMany.FunctionDelegate> lambda,
			int inputCount,
			int outputCount)
		{
			return new FunctionManyToMany(inputCount, outputCount, lambda);
		}
	}
}
