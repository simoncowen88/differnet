﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	internal static class LinqHelpers
	{
		internal static BinaryExpression ArrayIndex(
			System.Linq.Expressions.Expression array,
			int index)
		{
			return System.Linq.Expressions.Expression.ArrayIndex(
				array,
				Constant(index));
		}

		internal static ConstantExpression Constant<T>(
			T value)
		{
			return System.Linq.Expressions.Expression.Constant(value, typeof (T));
		}

		internal static MethodCallExpression CallStatic(
			Func<double, double> func,
			System.Linq.Expressions.Expression arg0)
		{
			var methodInfo = func.Method;
			return System.Linq.Expressions.Expression.Call(methodInfo, arg0);
		}

		internal static MethodCallExpression CallStatic(
			Func<double, double, double> func,
			System.Linq.Expressions.Expression arg0,
			System.Linq.Expressions.Expression arg1)
		{
			var methodInfo = func.Method;
			return System.Linq.Expressions.Expression.Call(methodInfo, arg0, arg1);
		}

		internal static MethodCallExpression CallStatic(
			Func<double, double, double, double> func,
			System.Linq.Expressions.Expression arg0,
			System.Linq.Expressions.Expression arg1,
			System.Linq.Expressions.Expression arg2)
		{
			var methodInfo = func.Method;
			return System.Linq.Expressions.Expression.Call(methodInfo, arg0, arg1, arg2);
		}

		internal static MethodCallExpression CallStatic(
			Func<double, double, double, double, double> func,
			System.Linq.Expressions.Expression arg0,
			System.Linq.Expressions.Expression arg1,
			System.Linq.Expressions.Expression arg2,
			System.Linq.Expressions.Expression arg3)
		{
			var methodInfo = func.Method;
			return System.Linq.Expressions.Expression.Call(methodInfo, arg0, arg1, arg2, arg3);
		}
	}
}
