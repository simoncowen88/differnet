﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	/// <summary>
	/// Represents a function with multiple inputs and a single output.
	/// </summary>
	public interface IFunctionManyTo1
	{
		/// <summary>
		/// Gets the number of inputs that this function requires.
		/// </summary>
		/// <value>
		/// The number of inputs that this function requires.
		/// </value>
		int InputCount { get; }

		/// <summary>
		/// Invokes the compiled expression with the specified input, assigning
		/// the result to the specified output.
		/// </summary>
		/// <param name="inputs">The inputs to the function.</param>
		/// <param name="output">A variable to be assigned the result of the function.</param>
		void Invoke(double[] inputs, out double output);

		/// <summary>
		/// Invokes the compiled expression with the specified inputs.
		/// </summary>
		/// <param name="inputs">The inputs to the function.</param>
		/// <returns>The result of the function.</returns>
		double Invoke(params double[] inputs);
	}

	internal partial class FunctionManyTo1 : IFunctionManyTo1
	{
		internal delegate void FunctionDelegate(double[] inputs, out double output);

		#region Fields

		private readonly int inputCount;
		private readonly FunctionDelegate invokeFunc;

		#endregion

		#region Constructors

		internal FunctionManyTo1(
			int inputCount, 
			Expression<FunctionDelegate> valueExpression)
		{
			this.inputCount = inputCount;
			invokeFunc = valueExpression.Compile();
		}

		#endregion

		#region Properties
		
		public int InputCount
		{
			get { return inputCount; }
		}

		#endregion

		#region Methods
		
		public double Invoke(params double[] inputs)
		{
			double output;
			invokeFunc(inputs, out output);
			return output;
		}

		public void Invoke(double[] inputs, out double output)
		{
			invokeFunc(inputs, out output);
		}

		#endregion
	}
}
