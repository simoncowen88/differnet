﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DifferNET.Compiled
{
	internal static class CompilerFactory
	{
		#region Fields

		private static ICompiler<IFunction1To1> function1To1Compiler;
		private static ICompiler<IFunction1ToMany> function1ToManyCompiler;
		private static ICompiler<IFunctionManyTo1> functionManyTo1Compiler;
		private static ICompiler<IFunctionManyToMany> functionManyToManyCompiler;

		#endregion

		#region Constructors

		static CompilerFactory()
		{
			SetFunction1To1CompilerToDefault();
			SetFunction1ToManyCompilerToDefault();
			SetFunctionManyTo1CompilerToDefault();
			SetFunctionManyToManyCompilerToDefault();
		}

		#endregion

		#region Methods

		internal static ICompiler<IFunction1To1> Function1To1Compiler
		{
			get { return function1To1Compiler; }
		}

		internal static ICompiler<IFunction1ToMany> Function1ToManyCompiler
		{
			get { return function1ToManyCompiler; }
		}

		internal static ICompiler<IFunctionManyTo1> FunctionManyTo1Compiler
		{
			get { return functionManyTo1Compiler; }
		}

		internal static ICompiler<IFunctionManyToMany> FunctionManyToManyCompiler
		{
			get { return functionManyToManyCompiler; }
		}

		internal static void SetFunction1To1CompilerToDefault()
		{
			function1To1Compiler = Compiler.Create(
				new SingleInputCompiler(),
				new SingleOutputCompiler(),
				new Function1To1Compiler());
		}

		internal static void SetFunction1ToManyCompilerToDefault()
		{
			function1ToManyCompiler = Compiler.Create(
				new ManyInputCompiler(),
				new SingleOutputCompiler(),
				new Function1ToManyCompiler());
		}

		internal static void SetFunctionManyTo1CompilerToDefault()
		{
			functionManyTo1Compiler = Compiler.Create(
				new ManyInputCompiler(),
				new SingleOutputCompiler(),
				new FunctionManyTo1Compiler());
		}

		internal static void SetFunctionManyToManyCompilerToDefault()
		{
			functionManyToManyCompiler = Compiler.Create(
				new ManyInputCompiler(),
				new ManyOutputCompiler(),
				new FunctionManyToManyCompiler());
		}

		#endregion

		#region Set methods for testing

		// FOR TESTING ONLY.

		[Conditional("DEBUG")]
		internal static void SetFunction1To1Compiler(
			ICompiler<IFunction1To1> _function1To1Compiler)
		{
			function1To1Compiler = _function1To1Compiler;
		}

		[Conditional("DEBUG")]
		internal static void SetFunction1ToManyCompiler(
			ICompiler<IFunction1ToMany> _function1ToManyCompiler)
		{
			function1ToManyCompiler = _function1ToManyCompiler;
		}

		[Conditional("DEBUG")]
		internal static void SetFunctionManyTo1Compiler(
			ICompiler<IFunctionManyTo1> _functionManyTo1Compiler)
		{
			functionManyTo1Compiler = _functionManyTo1Compiler;
		}

		[Conditional("DEBUG")]
		internal static void SetFunctionManyToManyCompiler(
			ICompiler<IFunctionManyToMany> _functionManyToManyCompiler)
		{
			functionManyToManyCompiler = _functionManyToManyCompiler;
		}

		#endregion
	}
}
