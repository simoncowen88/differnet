﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET.Compiled
{
	internal interface ILinqValue
	{
		System.Linq.Expressions.Expression Value { get; }

		void SetLinqExpression(
			IExpressionVisitor_GetLinqExpression linqExpressionVisitor,
			IMap<Expression, System.Linq.Expressions.Expression> implementations);
	}
}
