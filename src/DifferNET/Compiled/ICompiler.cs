﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET.Compiled
{
	internal interface ICompiler<out TFunction>
	{
		TFunction GetFunction(
			Input[] inputs,
			params Expression[] outputs);
	}
}
