﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	/// <summary>
	/// Represents a function with multiple inputs and multiple outputs.
	/// </summary>
	public interface IFunctionManyToMany
	{
		/// <summary>
		/// Gets the number of inputs that this function requires.
		/// </summary>
		/// <value>
		/// The number of inputs that this function requires.
		/// </value>
		int InputCount { get; }

		/// <summary>
		/// Gets the number of outputs that this function returns.
		/// </summary>
		/// <value>
		/// The number of outputs that this function returns.
		/// </value>
		int OutputCount { get; }

		/// <summary>
		/// Invokes the compiled expressions with the specified inputs, filling
		/// the passed output array with the outputs.
		/// </summary>
		/// <param name="inputs">The inputs to the function.</param>
		/// <param name="outputs">An array to be filled with the results of the function.</param>
		void Invoke(double[] inputs, double[] outputs);

		/// <summary>
		/// Invokes the compiled expressions with the specified inputs,
		/// returning an array of the outputs.
		/// </summary>
		/// <param name="inputs">The inputs to the function.</param>
		/// <returns>The results of the function.</returns>
		double[] Invoke(params double[] inputs);
	}

	internal partial class FunctionManyToMany : IFunctionManyToMany
	{
		internal delegate void FunctionDelegate(double[] inputs, double[] outputs);
		
		#region Fields

		private readonly int inputCount;
		private readonly int outputCount;
		private readonly FunctionDelegate invokeFunc;
		
		#endregion

		#region Constructors

		internal FunctionManyToMany(
			int inputCount,
			int outputCount,
			Expression<FunctionDelegate> valueExpression)
		{
			this.inputCount = inputCount;
			this.outputCount = outputCount;
			invokeFunc = valueExpression.Compile();
		}

		#endregion

		#region Properties

		public int InputCount
		{
			get { return inputCount; }
		}

		public int OutputCount
		{
			get { return outputCount; }
		} 

		#endregion

		#region Methods

		public void Invoke(double[] inputs, double[] outputs)
		{
			invokeFunc(inputs, outputs);
		}

		public double[] Invoke(params double[] inputs)
		{
			var values = new double[outputCount];
			invokeFunc(inputs, values);
			return values;
		}

		#endregion
	}
}
