﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	/// <summary>
	/// Represents a function with a single input and multiple outputs.
	/// </summary>
	public interface IFunction1ToMany
	{
		/// <summary>
		/// Gets the number of outputs that this function returns.
		/// </summary>
		/// <value>
		/// The number of outputs that this function returns.
		/// </value>
		int OutputCount { get; }

		/// <summary>
		/// Invokes the compiled expressions with the specified input, filling
		/// the passed output array with the outputs.
		/// </summary>
		/// <param name="input">The input to the function.</param>
		/// <param name="outputs">An array to be filled with the results of the function.</param>
		void Invoke(double input, double[] outputs);

		/// <summary>
		/// Invokes the compiled expressions with the specified input,
		/// returning an array of the outputs.
		/// </summary>
		/// <param name="input">The input to the function.</param>
		/// <returns>The results of the function.</returns>
		double[] Invoke(double input);
	}

	internal partial class Function1ToMany : IFunction1ToMany
	{
		internal delegate void FunctionDelegate(double inputs, double[] outputs);
		
		#region Fields

		private readonly int outputCount;
		private readonly FunctionDelegate invokeFunc;
		
		#endregion

		#region Constructors

		internal Function1ToMany(
			int outputCount,
			Expression<FunctionDelegate> valueExpression)
		{
			this.outputCount = outputCount;
			invokeFunc = valueExpression.Compile();
		}

		#endregion

		#region Properties

		public int OutputCount
		{
			get { return outputCount; }
		} 

		#endregion

		#region Methods

		public void Invoke(double input, double[] outputs)
		{
			invokeFunc(input, outputs);
		}

		public double[] Invoke(double input)
		{
			var values = new double[outputCount];
			invokeFunc(input, values);
			return values;
		}

		#endregion
	}
}
