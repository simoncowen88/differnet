﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DifferNET.Compiled
{
	internal interface IOutputCompiler
	{
		Type OutputType { get; }

		IEnumerable<System.Linq.Expressions.Expression> GetOutputExpressions(
			Expression[] outputs,
			ParameterExpression outputExpression,
			IMap<Expression, System.Linq.Expressions.Expression> valueMap);
	}

	internal class SingleOutputCompiler : IOutputCompiler
	{
		public Type OutputType
		{
			get { return typeof (double).MakeByRefType(); }
		}

		public IEnumerable<System.Linq.Expressions.Expression> GetOutputExpressions(
			Expression[] outputs,
			ParameterExpression outputExpression,
			IMap<Expression, System.Linq.Expressions.Expression> valueMap)
		{
			var output = outputs.Single();

			var rootValue = valueMap[output];
			var outputAssignment = System.Linq.Expressions.Expression.Assign(
				outputExpression,
				rootValue);

			yield return outputAssignment;
		}
	}

	internal class ManyOutputCompiler : IOutputCompiler
	{
		public Type OutputType
		{
			get { return typeof (double[]); }
		}

		public IEnumerable<System.Linq.Expressions.Expression> GetOutputExpressions(
			Expression[] outputs,
			ParameterExpression outputExpression,
			IMap<Expression, System.Linq.Expressions.Expression> valueMap)
		{
			var outputArrayAssignments = outputs
				.Select((output, index) =>
					GetOutputAssign(valueMap, output, index, outputExpression));

			return outputArrayAssignments;
		}

		private static BinaryExpression GetOutputAssign(
			IMap<Expression, System.Linq.Expressions.Expression> valueMap,
			Expression output,
			int index,
			System.Linq.Expressions.Expression outputArray)
		{
			var indexConstant = System.Linq.Expressions.Expression.Constant(index, typeof (int));
			var value = valueMap[output];

			var arrayAccess = System.Linq.Expressions.Expression.ArrayAccess(
				outputArray,
				indexConstant);

			return System.Linq.Expressions.Expression.Assign(
				arrayAccess,
				value);
		}
	}
}
