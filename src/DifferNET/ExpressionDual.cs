﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using LX = System.Linq.Expressions;

namespace DifferNET
{
	internal class ExpressionDual
	{
		#region Fields

		private readonly Expression expression;
		private Expression derivative;

		#endregion

		#region Constructors

		internal ExpressionDual(Expression expression)
			: this(expression, Constant.Zero)
		{
		}

		internal ExpressionDual(Expression expression, Expression initialDerivative)
		{
			this.expression = expression;
			derivative = initialDerivative;
		}

		#endregion

		#region Properties

		internal Expression Derivative
		{
			get { return derivative; }
		}

		#endregion

		#region Methods

		internal void PushDerivativesToPrecedents(
			IExpressionVisitor_GetDerivatives derivativesVisitor,
			IMap<Expression, ExpressionDual> expressionDuals)
		{
			var expressionDerivatives = GetDerivatives(derivativesVisitor);

			CheckDerivatives(expressionDerivatives);

			// chain rule time:
			// d/dx f(g(x)) == g'(x)f'(g(x))

			foreach (var expressionDerivative in expressionDerivatives)
			{
				// get the dual of the precedent this derivative is wrt
				var precedentDual = expressionDuals[expressionDerivative.Precedent];

				// this.derivative == g'
				// expressionDerivative.Derivative == f'
				precedentDual.derivative += derivative*expressionDerivative.Derivative;
			}
		}

		private ReadOnlyCollection<ExpressionDerivative> GetDerivatives(
			IExpressionVisitor_GetDerivatives derivativesVisitor)
		{
			return expression
				.Accept(derivativesVisitor, Unit.Default)
				.ToList()
				.AsReadOnly();
		}

		private void CheckDerivatives(
			IReadOnlyCollection<ExpressionDerivative> derivativesList)
		{
			// a precedent was mentioned in the derivatives more than once
			var duplicatePrecedent = 
				ContainsDuplicates(derivativesList.Select(o => o.Precedent));
			if (duplicatePrecedent)
				throw new Exception(ResourceStrings.DuplicatePrecedent);

			// a precedent in the derivatives was not a precedent of the original expression
			var unexpectedPrecedent = derivativesList
				.Any(d => !expression.Precedents.Contains(d.Precedent));
			if (unexpectedPrecedent)
				throw new Exception(ResourceStrings.UnexpectedPrecedent);

			// a precedent of the original expression was not mentioned in the derivatives
			var unmentionedPrecedent = expression.Precedents
				.Any(o => !derivativesList.Select(d => d.Precedent).Contains(o));
			if (unmentionedPrecedent)
				throw new Exception(ResourceStrings.PrecedentUnmentioned);
		}

		private static bool ContainsDuplicates<T>(IEnumerable<T> source)
		{
			var hashSet = new HashSet<T>();
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var item in source)
			{
				if (!hashSet.Add(item))
					return true;
			}
			return false;
		}

		#endregion
	}
}
