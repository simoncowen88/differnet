﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Step : Expression
	{
		#region Fields

		private readonly double leftValue;
		private readonly double midValue;
		private readonly double rightValue;

		#endregion

		#region Constructors

		private Step(
			Expression expression,
			double leftValue,
			double midValue,
			double rightValue)
			: base(expression)
		{
			this.leftValue = leftValue;
			this.midValue = midValue;
			this.rightValue = rightValue;
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		internal double LeftValue
		{
			get { return leftValue; }
		}

		internal double MidValue
		{
			get { return midValue; }
		}

		internal double RightValue
		{
			get { return rightValue; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("step({0},[{1},{2},{3}])", X, LeftValue, MidValue, RightValue);
		}

		#endregion

		#region Static
		
		internal static Expression Of(
			Expression expression,
			double leftValue,
			double midValue,
			double rightValue)
		{
			if (expression is Constant)
			{
				var constantValue = Apply(
					expression.ConstantValue(),
					leftValue,
					midValue,
					rightValue);

				return Constant.Of(constantValue);
			}

			return new Step(expression, leftValue, midValue, rightValue);
		}

		internal static double Apply(
			double input,
			double leftValue,
			double midValue,
			double rightValue)
		{
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (input == 0d)
				return midValue;

			if (input > 0)
				return rightValue;

			if (input < 0)
				return leftValue;

			//if (double.IsNaN(input))
			return double.NaN;
		}

		#endregion
	}
}
