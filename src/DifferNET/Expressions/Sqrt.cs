﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	// Math.Pow has HORRIBLE performance! Math.Sqrt is significantly faster.

	internal sealed class Sqrt : Expression
	{
		#region Fields

		#endregion

		#region Constructors

		private Sqrt(Expression expression)
			: base(expression)
		{
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("sqrt({0})", X);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression expression)
		{
			if (expression is Constant)
				return Constant.Of(Math.Sqrt(expression.ConstantValue()));

			return new Sqrt(expression);
		}

		#endregion
	}
}
