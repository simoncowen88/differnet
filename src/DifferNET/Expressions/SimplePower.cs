﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class SimplePower : Expression
	{
		#region Fields

		private readonly double exponent;

		#endregion

		#region Constructors

		private SimplePower(Expression x, double exponent)
			: base(x)
		{
			this.exponent = exponent;
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		internal double Exponent
		{
			get { return exponent; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("({0}^{1})", X, exponent);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression x, double exponent)
		{
			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (exponent == 1d)
				return x;

			if (x is Constant)
				return Constant.Of(Math.Pow(x.ConstantValue(), exponent));

			return new SimplePower(x, exponent);
		}

		#endregion
	}
}
