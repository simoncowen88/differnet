﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Sum : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Sum(Expression[] expressions)
			: base(expressions)
		{
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("({0})", string.Join<Expression>("+", Precedents));
		}

		#endregion

		#region Static

		internal static Expression Of(params Expression[] expressions)
		{
			// There should be no sub-sub expressions, as the we must always 
			// come through this method.
			expressions = Helpers.ExpandSubExpressionsOfType<Sum>(expressions);
			expressions = Helpers.CollapseSumConstants(expressions);

			if (expressions.Length == 0)
				return Constant.Zero;
			if (expressions.Length == 1)
				return expressions[0];
			
			return new Sum(expressions);
		}

		#endregion
	}
}
