﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Exp : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Exp(Expression exponent)
			: base(exponent)
		{
		}

		#endregion

		#region Properties

		internal Expression Exponent
		{
			get { return Precedents[0]; }
		}		
		
		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("(e^{0})", Exponent);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression exponent)
		{
			if (exponent is Constant)
				return Constant.Of(Math.Exp(exponent.ConstantValue()));

			return new Exp(exponent);
		}

		#endregion
	}
}
