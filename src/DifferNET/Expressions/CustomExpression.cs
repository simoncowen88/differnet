using System.Collections.Generic;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	/// <summary>
	/// Provides the base class from which custom expression types may be derived.
	/// </summary>
	public abstract class CustomExpression : Expression
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CustomExpression"/> class,
		/// with the specified precedents.
		/// </summary>
		/// <param name="precedents">The precedents of this expression.</param>
		protected CustomExpression(params Expression[] precedents)
			: base(precedents)
		{
		}

		internal sealed override TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		// these methods are to allow custom expressions while using the visitor pattern

		/// <summary>
		/// When overridden, returns the derivatives of this expression with
		/// respect to each of its precedents.
		/// </summary>
		/// <returns>
		/// The derivatives of this expression with respect to each of its precedents.
		/// </returns>
		protected internal abstract IEnumerable<ExpressionDerivative> GetDerivativeExpression();

		/// <summary>
		/// When overridden, returns a Linq expression for this expression.
		/// </summary>
		/// <param name="implementations">
		/// A map of all preceding expressions to their Linq expressions.
		/// </param>
		/// <returns>
		/// A Linq expression for this expression.
		/// </returns>
		protected internal abstract System.Linq.Expressions.Expression GetLinqExpression(
			IMap<Expression, System.Linq.Expressions.Expression> implementations);
	}
}