﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	// TODO: Could i not just check for a constant base when visiting / deriv'ing ??

	internal sealed class SimpleLog : Expression
	{
		#region Fields

		private readonly double logBase;
		
		#endregion

		#region Constructors

		private SimpleLog(Expression expression, double logBase)
			: base(expression)
		{
			this.logBase = logBase;
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		internal double Base
		{
			get { return logBase; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("log({0},{1})", X, logBase);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression expression, double logBase)
		{
			if (expression is Constant)
				return Constant.Of(Math.Log(expression.ConstantValue(), logBase));

			return new SimpleLog(expression, logBase);
		}

		#endregion
	}
}
