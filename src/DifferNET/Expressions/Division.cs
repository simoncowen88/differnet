﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Division : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Division(Expression numerator, Expression denominator)
			: base(numerator, denominator)
		{
		}

		#endregion

		#region Properties

		internal Expression Numerator
		{
			get { return Precedents[0]; }
		}

		internal Expression Denominator
		{
			get { return Precedents[1]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("({0}/{1})", Numerator, Denominator);
		}

		//public static Func<Expression, Expression, Expression> GetFunc()
		//{
		//	return (x, y) => new Division(x, y);
		//}

		#endregion

		#region Static

		internal static Expression Of(
			Expression numerator, Expression denominator)
		{
			if (denominator == Constant.One)
				return numerator;
			if (numerator is Constant && denominator is Constant)
				return Constant.Of(numerator.ConstantValue() / denominator.ConstantValue());

			return new Division(numerator, denominator);
		}

		#endregion
	}
}
