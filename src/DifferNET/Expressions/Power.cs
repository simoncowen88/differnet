﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Power : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Power(Expression x, Expression exponent)
			: base(x, exponent)
		{
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		internal Expression Exponent
		{
			get { return Precedents[1]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("({0}**({1}))", X, Exponent);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression x, Expression exponent)
		{
			if (exponent is Constant)
				return EMath.Pow(x, exponent.ConstantValue());

			return new Power(x, exponent);
		}

		#endregion
	}
}
