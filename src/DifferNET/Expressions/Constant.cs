﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	/// <summary>
	/// Represents an expression that is a constant.
	/// </summary>
	public sealed class Constant : Expression
	{
		#region Fields

		private readonly double value;

		#endregion

		#region Constructors

		private Constant(double value)
		{
			this.value = value;
		}

		#endregion

		#region Properties

		/// <summary>Gets the value of this constant.</summary>
		/// <value>The value of this constant.</value>
		public double Value
		{
			get { return value; }
		} 

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return string.Format("{0}", value);
		}

		#endregion

		#region Static

		/// <summary>
		/// A contant of zero.
		/// </summary>
		public static readonly Constant Zero = new Constant(0d);

		/// <summary>
		/// A contant of one.
		/// </summary>
		public static readonly Constant One = new Constant(1d);

		/// <summary>
		/// A contant of minus one.
		/// </summary>
		public static readonly Constant MinusOne = new Constant(-1d);

		/// <summary>
		/// Creates a contant of the specified value.
		/// </summary>
		/// <param name="value">The value of the contant to be created.</param>
		/// <returns>A constant with the speicifed value.</returns>
		public static Constant Of(double value)
		{
			// This allow us to use reference equality to check for 0 and +-1

			// ReSharper disable CompareOfFloatsByEqualityOperator
			if (value == 0d)
				return Zero;
			if (value == 1d)
				return One;
			if (value == -1d)
				return MinusOne;
			// ReSharper restore CompareOfFloatsByEqualityOperator

			return new Constant(value);
		}

		#endregion

		#region Operator overrides

		/// <summary>
		/// Converts the specified double to a <see cref="DifferNET.Constant" />.
		/// </summary>
		public static implicit operator Constant(double value)
		{
			return Of(value);
		}

		/// <summary>Negates the specified constant.</summary>
		public static Constant operator -(Constant expression)
		{
			return Of(-expression.value);
		}

		/// <summary>Sums the specified constant.</summary>
		public static Constant operator +(Constant left, Constant right)
		{
			return Of(left.value + right.value);
		}

		/// <summary>Multiplies the specified constant.</summary>
		public static Constant operator *(Constant left, Constant right)
		{
			return Of(left.value * right.value);
		}

		/// <summary>Subtracts the specified constant.</summary>
		public static Constant operator -(Constant left, Constant right)
		{
			return Of(left.value - right.value);
		}

		/// <summary>Divides the specified constant.</summary>
		public static Constant operator /(Constant left, Constant right)
		{
			return Of(left.value / right.value);
		}

		#endregion
	}
}
