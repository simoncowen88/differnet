﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class ArcTan : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private ArcTan(Expression x)
			: base(x)
		{
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("atan({0})", X);
		}

		#endregion

		#region Static

		internal static Expression Of(Expression x)
		{
			if (x is Constant)
				return Constant.Of(Math.Atan(x.ConstantValue()));

			return new ArcTan(x);
		}

		#endregion
	}
}
