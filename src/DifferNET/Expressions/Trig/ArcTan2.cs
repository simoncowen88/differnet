﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	// f = arctan (y/x)
	internal sealed class ArcTan2 : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private ArcTan2(Expression y, Expression x)
			: base(y, x)
		{
		}

		#endregion

		#region Properties

		internal Expression Y
		{
			get { return Precedents[0]; }
		}

		internal Expression X
		{
			get { return Precedents[1]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("atan2({0}, {1})", Y, X);
		}

		#endregion

		#region Static

		internal static Expression Of(Expression y, Expression x)
		{
			if (x is Constant && y is Constant)
				return Constant.Of(Math.Atan2(y.ConstantValue(), x.ConstantValue()));

			return new ArcTan2(y, x);
		}

		#endregion
	}
}
