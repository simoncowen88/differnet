﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Product : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Product(Expression[] expressions)
			: base(expressions)
		{
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}
		
		public override string ToString()
		{
			return string.Format("({0})", string.Join<Expression>("*", Precedents));
		}

		#endregion

		#region Static

		internal static Expression Of(params Expression[] expressions)
		{
			expressions = Helpers.ExpandSubExpressionsOfType<Product>(expressions);
			expressions = Helpers.CollapseProductConstants(expressions);

			// value != Constant.Zero when any expression == Constant.Zero
			// e.g. if any expression has a singularity.

			if (expressions.Length == 0)
				return Constant.One;
			if (expressions.Length == 1)
				return expressions[0];

			return new Product(expressions);
		}

		#endregion
	}
}
