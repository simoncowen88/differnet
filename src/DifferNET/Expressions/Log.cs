﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Log : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Log(Expression expression, Expression logBase)
			: base(expression, logBase)
		{
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		internal Expression Base
		{
			get { return Precedents[1]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("log({0},{1})", X, Base);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression expression, Expression logBase)
		{
			if (logBase is Constant)
				return EMath.Log(expression, logBase.ConstantValue());

			return new Log(expression, logBase);
		}

		#endregion
	}
}
