﻿using System;
using System.Collections.Generic;
using System.Linq;
using DifferNET.Compiled;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	/// <summary>
	/// Provides the base class from which classes that represent expressions
	/// are derived.
	/// </summary>
	public abstract class Expression
	{
		#region Fields

		private readonly Expression[] precedents;

		#endregion

		#region Constructors
		
		/// <summary>
		/// Initializes a new instance of the <see cref="Expression" /> class,
		/// with the specified precedents.
		/// 
		/// This internal constructor forces all external inheritors to go through
		/// <see cref="CustomExpression" />.
		/// </summary>
		/// <param name="precedents">The precedents of this expression.</param>
		internal Expression(params Expression[] precedents)
		{
			this.precedents = precedents;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Returns the precendents of this expression.
		/// </summary>
		/// <returns>The precendents of this expression.</returns>
		protected internal Expression[] Precedents
		{
			get { return precedents; }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates a compiled function for this expression.
		/// </summary>
		/// <param name="inputs">The inputs for this expression.</param>
		/// <returns>
		/// A compiled function for this expression, using the ordering of the 
		/// specified inputs for the ordering of arguments to the function.
		/// </returns>
		public IFunctionManyTo1 Compile(params Input[] inputs)
		{
			var compiler = CompilerFactory.FunctionManyTo1Compiler;
			return compiler.GetFunction(inputs, new[] {this});
		}

		/// <summary>
		/// Creates a compiled function for this expression.
		/// </summary>
		/// <param name="input">The input for this expression.</param>
		/// <returns>A compiled function for this expression.</returns>
		public IFunction1To1 Compile(Input input)
		{
			var compiler = CompilerFactory.Function1To1Compiler;
			return compiler.GetFunction(new[] {input}, new[] {this});
		}

		/// <summary>Gets the derivatives of this expression.</summary>
		/// <param name="inputs">The inputs for this expression.</param>
		/// <returns>
		/// A map from each input to the derivative with respect that input.
		/// </returns>
		public IMap<Input, Expression> GetDerivatives(params Input[] inputs)
		{
			var differentiator = DifferentiatorFactory.GetDifferentiator();
			return differentiator.CreateDerivatives(inputs, this);
		}

		/// <summary>Gets the derivative of this expression.</summary>
		/// <param name="input">The input for this expression.</param>
		/// <returns>The derivative of this expression.</returns>
		public Expression GetDerivative(Input input)
		{
			var inputs = new[] {input};
			var derivatives = GetDerivatives(inputs);
			return derivatives[input];
		}

		/// <summary>
		/// Creates a compiled function for the derivative of this expression.
		/// </summary>
		/// <param name="input">The input for this expression.</param>
		/// <returns>
		/// A compiled function for the derivative of this expression.
		/// </returns>
		public IFunction1To1 CompileDerivative(Input input)
		{
			var derivative = GetDerivative(input);
			return derivative.Compile(input);
		}

		/// <summary>
		/// Creates a compiled function for the derivatives of this expression.
		/// </summary>
		/// <param name="inputs">The inputs for this expression.</param>
		/// <returns>
		/// A compiled function for the derivatives of this expression.
		/// </returns>
		public IFunctionManyToMany CompileDerivatives(params Input[] inputs)
		{
			var derivativesMap = GetDerivatives(inputs);

			var derivatives = inputs
				.Select(input => derivativesMap[input])
				.ToArray();

			return derivatives.Compile(inputs);
		}

		internal abstract TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter);
		
		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return string.Format("f({0})", string.Join<Expression>(",", precedents));
		}

		#endregion
		
		#region Operator overrides

		/// <summary>
		/// Converts the specified double to a <see cref="DifferNET.Constant" />.
		/// </summary>
		public static implicit operator Expression(double value)
		{
			return Constant.Of(value);
		}

		/// <summary>Negates the specified expression.</summary>
		public static Expression operator -(Expression expression)
		{
			return EMath.Negate(expression);
		}

		/// <summary>Sums the specified expressions.</summary>
		public static Expression operator +(Expression left, Expression right)
		{
			return EMath.Sum(left, right);
		}
		/// <summary>Sums the specified expressions.</summary>
		public static Expression operator +(Expression left, double right)
		{
			return left + Constant.Of(right);
		}
		/// <summary>Sums the specified expressions.</summary>
		public static Expression operator +(double left, Expression right)
		{
			return Constant.Of(left) + right;
		}

		/// <summary>Multiplies the specified expressions.</summary>
		public static Expression operator *(Expression left, Expression right)
		{
			return EMath.Product(left, right);
		}
		/// <summary>Multiplies the specified expressions.</summary>
		public static Expression operator *(Expression left, double right)
		{
			return left * Constant.Of(right);
		}
		/// <summary>Multiplies the specified expressions.</summary>
		public static Expression operator *(double left, Expression right)
		{
			return Constant.Of(left) * right;
		}

		/// <summary>Subtracts the specified expressions.</summary>
		public static Expression operator -(Expression left, Expression right)
		{
			return EMath.Subtract(left, right);
		}
		/// <summary>Subtracts the specified expressions.</summary>
		public static Expression operator -(Expression left, double right)
		{
			return left - Constant.Of(right);
		}
		/// <summary>Subtracts the specified expressions.</summary>
		public static Expression operator -(double left, Expression right)
		{
			return Constant.Of(left) - right;
		}

		/// <summary>Divides the specified expressions.</summary>
		public static Expression operator /(Expression left, Expression right)
		{
			return EMath.Divide(left, right);
		}
		/// <summary>Divides the specified expressions.</summary>
		public static Expression operator /(Expression left, double right)
		{
			return left / Constant.Of(right);
		}
		/// <summary>Divides the specified expressions.</summary>
		public static Expression operator /(double left, Expression right)
		{
			return Constant.Of(left) / right;
		}

		#endregion
	}
}
