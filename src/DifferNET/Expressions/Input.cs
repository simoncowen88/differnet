﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using LX = System.Linq.Expressions;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	/// <summary>
	/// Represents an expression that is an input.
	/// </summary>
	public sealed class Input : Expression
	{
		#region Fields

		private readonly string name;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="Input"/> class.
		/// </summary>
		/// <param name="name">The name for this input.</param>
		public Input(string name)
		{
			this.name = name;
		}

		#endregion

		#region Properties

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(
			IExpressionVisitor<TReturn, TParameter> visitor,
			TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return name;
		}

		#endregion
	}
}
