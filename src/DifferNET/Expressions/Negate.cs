﻿using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace DifferNET
{
	internal sealed class Negate : Expression
	{
		#region Fields
		#endregion

		#region Constructors

		private Negate(Expression expression)
			: base(expression)
		{
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		#endregion

		#region Methods

		internal override TReturn Accept<TReturn, TParameter>(IExpressionVisitor<TReturn, TParameter> visitor, TParameter parameter)
		{
			return visitor.Visit(parameter, this);
		}

		public override string ToString()
		{
			return string.Format("-{0}", X);
		}

		#endregion

		#region Static

		internal static Expression Of(
			Expression expression)
		{
			if (expression is Constant)
				return Constant.Of(-expression.ConstantValue());

			return new Negate(expression);
		}

		#endregion
	}
}
