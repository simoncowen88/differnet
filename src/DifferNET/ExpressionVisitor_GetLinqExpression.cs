﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DifferNET.Compiled;
using LX = System.Linq.Expressions;

namespace DifferNET
{
	internal interface IExpressionVisitor_GetLinqExpression
		: IExpressionVisitor<LX.Expression, IMap<Expression, LX.Expression>>
	{
	}

	internal sealed class ExpressionVisitor_GetLinqExpression
		: IExpressionVisitor_GetLinqExpression
	{
		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Input expression)
		{
			throw new NotImplementedException();
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Constant expression)
		{
			return LinqHelpers.Constant(expression.Value);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Abs expression)
		{
			return LinqHelpers.CallStatic(Math.Abs, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Division expression)
		{
			return LX.Expression.Divide(
				implementations[expression.Numerator],
				implementations[expression.Denominator]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Exp expression)
		{
			return LinqHelpers.CallStatic(Math.Exp, implementations[expression.Exponent]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Ln expression)
		{
			return LinqHelpers.CallStatic(Math.Log, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Log expression)
		{
			return LinqHelpers.CallStatic(
				Math.Log,
				implementations[expression.X],
				implementations[expression.Base]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Negate expression)
		{
			return LX.Expression.Negate(implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Power expression)
		{
			return LinqHelpers.CallStatic(
				Math.Pow,
				implementations[expression.X],
				implementations[expression.Exponent]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Product expression)
		{
			var precedents = expression.Precedents;

			if (precedents == null ||
			    precedents.Length == 0)
				return LinqHelpers.Constant(1d);

			if (precedents.Length == 1)
				return implementations[precedents[0]];

			return GetProduct(precedents.Select(e => implementations[e]));
		}

		private static LX.Expression GetProduct(IEnumerable<LX.Expression> expressions)
		{
			var expressionsArray = expressions.ToArray();

			if (expressions == null ||
			    expressionsArray.Length == 0)
				return LinqHelpers.Constant(1d);

			if (expressionsArray.Length == 1)
				return expressionsArray[0];

			LX.BinaryExpression productAccumulator =
				LX.Expression.Multiply(expressionsArray[0], expressionsArray[1]);

			for (int i = 2; i < expressionsArray.Length; i++)
			{
				productAccumulator =
					LX.Expression.Multiply(productAccumulator, expressionsArray[i]);
			}

			return productAccumulator;
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			SimpleLog expression)
		{
			return LinqHelpers.CallStatic(
				Math.Log,
				implementations[expression.X],
				LinqHelpers.Constant(expression.Base));
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			SimplePower expression)
		{
			// NOT CORRECT. Value is NOT 1d at 0d.
			//if (exponent == 0d) { return LXHelpers.Constant(1d); }

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (expression.Exponent == 1d)
			{
				return implementations[expression.X];
			}

			return LinqHelpers.CallStatic(
				Math.Pow,
				implementations[expression.X],
				LinqHelpers.Constant(expression.Exponent));
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Sqrt expression)
		{
			// Math.Pow has HORRIBLE performance! Math.Sqrt is significantly faster.

			return LinqHelpers.CallStatic(Math.Sqrt, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Step expression)
		{
			// NaN => NaN
			// 0   => midVal
			// >0  => rightVal
			// <0  => leftVal

			return LinqHelpers.CallStatic(
				Step.Apply,
				implementations[expression.X],
				LinqHelpers.Constant(expression.LeftValue),
				LinqHelpers.Constant(expression.MidValue),
				LinqHelpers.Constant(expression.RightValue));

			//var xVal = implementations[X];

			//var isNaNMethodInfo = ((Func<double, bool>)double.IsNaN).Method;
			//var isNaN = LX.Expression.Call(isNaNMethodInfo, xVal);

			//var zeroVal = LXHelpers.Constant(0d);
			//var nanVal = LXHelpers.Constant(double.NaN);
			//var leftVal = LXHelpers.Constant(leftValue);
			//var rightVal = LXHelpers.Constant(rightValue);
			//var midVal = LXHelpers.Constant(midValue);

			//return
			//	LX.Expression.Condition(
			//		isNaN,
			//		nanVal, // if NaN
			//		LX.Expression.Condition(
			//			LX.Expression.Equal(implementations[X], zeroVal),
			//			midVal, // if == 0
			//			LX.Expression.Condition(
			//				LX.Expression.GreaterThan(implementations[X], zeroVal),
			//				rightVal, // if > 0
			//				leftVal // if < 0
			//				)));

			//LX.Expression<Func<double, double>> stepFunc =
			//	(d) => double.IsNaN(d) ? double.NaN : (
			//				d == 0
			//				? midValue
			//				: (
			//					d > 0
			//					? rightValue
			//					: leftValue));
			//return LX.Expression.Invoke(stepFunc, implementations[X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Sum expression)
		{
			var precedents = expression.Precedents;

			if (precedents == null || precedents.Length == 0)
				return LinqHelpers.Constant(0d);

			if (precedents.Length == 1)
				return implementations[precedents[0]];

			LX.BinaryExpression addAccumulator = LX.Expression.Add(
				implementations[precedents[0]],
				implementations[precedents[1]]);

			for (int i = 2; i < precedents.Length; i++)
			{
				addAccumulator = LX.Expression.Add(
					addAccumulator,
					implementations[precedents[i]]);
			}

			return addAccumulator;
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcCos expression)
		{
			return LinqHelpers.CallStatic(Math.Acos, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcCosec expression)
		{
			var oneOverX = LX.Expression.Divide(
				LinqHelpers.Constant(1d),
				implementations[expression.X]);

			return LinqHelpers.CallStatic(Math.Asin, oneOverX);

			//return (Model.Asin(1d / X)).GetLinqExpression(implementations);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcCot expression)
		{
			var oneOverX = LX.Expression.Divide(
				LinqHelpers.Constant(1d),
				implementations[expression.X]);

			return LinqHelpers.CallStatic(Math.Atan, oneOverX);

			// We would need to add (1d / X) to implementations for this to work... :(
			//return GetLinqExpressionFor(Model.Atan(1d / X), implementations);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcSec expression)
		{
			var oneOverX = LX.Expression.Divide(
				LinqHelpers.Constant(1d),
				implementations[expression.X]);

			return LinqHelpers.CallStatic(Math.Acos, oneOverX);

			//return (Model.Acos(1d / X)).GetLinqExpression(implementations);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcSin expression)
		{
			return LinqHelpers.CallStatic(Math.Asin, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcTan expression)
		{
			return LinqHelpers.CallStatic(Math.Atan, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			ArcTan2 expression)
		{
			return LinqHelpers.CallStatic(
				Math.Atan2,
				implementations[expression.Y],
				implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Cos expression)
		{
			return LinqHelpers.CallStatic(Math.Cos, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Cosec expression)
		{
			var sinX = LinqHelpers.CallStatic(Math.Sin, implementations[expression.X]);

			return LX.Expression.Divide(
				LinqHelpers.Constant(1d),
				sinX);

			//return (1d / Model.Sin(X)).GetLinqExpression(implementations);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Cosh expression)
		{
			return LinqHelpers.CallStatic(Math.Cosh, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Cot expression)
		{
			var tanX = LinqHelpers.CallStatic(Math.Tan, implementations[expression.X]);
			// = Model.Tan(X).GetLinqExpression(implementations); // would work as simply X

			return LX.Expression.Divide(
				LinqHelpers.Constant(1d),
				tanX);

			//return (1d / Model.Tan(X)).GetLinqExpression(implementations);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Sec expression)
		{
			var cosX = LinqHelpers.CallStatic(Math.Cos, implementations[expression.X]);

			return LX.Expression.Divide(
				LinqHelpers.Constant(1d),
				cosX);

			//return (1d / Model.Cos(X)).GetLinqExpression(implementations);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Sin expression)
		{
			return LinqHelpers.CallStatic(Math.Sin, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Sinh expression)
		{
			return LinqHelpers.CallStatic(Math.Sinh, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Tan expression)
		{
			return LinqHelpers.CallStatic(Math.Tan, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			Tanh expression)
		{
			return LinqHelpers.CallStatic(Math.Tanh, implementations[expression.X]);
		}

		public LX.Expression Visit(
			IMap<Expression, LX.Expression> implementations,
			CustomExpression expression)
		{
			var linqExpression = expression.GetLinqExpression(implementations);

			if (linqExpression == null)
				throw new Exception(ResourceStrings.GetLinqExpressionReturnedNull);

			return linqExpression;
		}
	}
}
