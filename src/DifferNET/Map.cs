﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DifferNET
{
	// TODO: Very inneficient, replace with bcl immutable collections...

	/// <summary>
	/// Represents a readonly collection of keys and values.
	/// </summary>
	/// <typeparam name="TKey">The type of the keys.</typeparam>
	/// <typeparam name="TValue">The type of the values.</typeparam>
	public interface IMap<in TKey, out TValue>
	{
		/// <summary>
		/// Gets the value associated with the specified key.
		/// </summary>
		/// <param name="key">The key of the value to get.</param>
		/// <returns>The value associated with the specified key.</returns>
		TValue this[TKey key] { get; }
	}

	internal class Map<TKey, TValue> : IMap<TKey, TValue>
	{
		#region Fields

		private static readonly Map<TKey, TValue> empty;
		private readonly Dictionary<TKey, TValue> dict;

		#endregion

		#region Constructors

		static Map()
		{
			empty = new Map<TKey, TValue>();
		}

		internal Map()
		{
			dict = new Dictionary<TKey, TValue>();
		}

		internal Map(IDictionary<TKey, TValue> initDict)
		{
			dict = new Dictionary<TKey, TValue>(initDict);
		}

		#endregion

		#region Properties

		public static Map<TKey, TValue> Empty
		{
			get { return empty; }
		}

		public TValue this[TKey key]
		{
			get { return dict[key]; }
		}

		#endregion

		#region Methods

		internal Map<TKey, TValue> With(TKey key, TValue value)
		{
			var newDict = new Dictionary<TKey, TValue>(dict);
			newDict[key] = value;
			return new Map<TKey, TValue>(newDict);
		}

		internal bool TryGetValue(TKey key, out TValue value)
		{
			return dict.TryGetValue(key, out value);
		}

		#endregion
	}
}
