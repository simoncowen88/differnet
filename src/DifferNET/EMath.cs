﻿using System;
using System.Collections.Generic;
using System.Linq;
using DifferNET.Annotations;

namespace DifferNET
{
	/// <summary>
	/// Contains static methods for mathematical functions, much like the built-in
	/// <see cref="System.Math"/> class.
	/// </summary>
	[PublicAPI]
	[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
	public static class EMath
	{
		// We only do very simple optimisations here, e.g. constant-folding, 
		// so as not to confuse the user.

		/// <summary>Returns the sum of the specified expressions.</summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns></returns>
		public static Expression Sum(params Expression[] expressions)
		{
			return DifferNET.Sum.Of(expressions);
		}

		/// <summary>Returns the sum of the specified expressions.</summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns></returns>
		public static Expression Sum(IEnumerable<Expression> expressions)
		{
			return Sum(expressions.ToArray());
		}

		/// <summary>Returns the sum of the specified expressions.</summary>
		/// <param name="left">An expression.</param>
		/// <param name="right">An expression.</param>
		/// <returns></returns>
		public static Expression Sum(Expression left, Expression right)
		{
			return Sum(new[] { left, right });
		}

		/// <summary>Subtracts the specified expressions.</summary>
		/// <param name="left">An expression.</param>
		/// <param name="right">An expression.</param>
		/// <returns></returns>
		public static Expression Subtract(Expression left, Expression right)
		{
			return Sum(left, Negate(right));
		}

		/// <summary>Returns the product of the specified expressions.</summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns></returns>
		public static Expression Product(params Expression[] expressions)
		{
			return DifferNET.Product.Of(expressions);
		}

		/// <summary>Returns the product of the specified expressions.</summary>
		/// <param name="expressions">The expressions.</param>
		/// <returns></returns>
		public static Expression Product(IEnumerable<Expression> expressions)
		{
			return Product(expressions.ToArray());
		}

		/// <summary>Returns the product of the specified expressions.</summary>
		/// <param name="left">An expression.</param>
		/// <param name="right">An expression.</param>
		/// <returns></returns>
		public static Expression Product(Expression left, Expression right)
		{
			return Product(new[] { left, right });
		}

		/// <summary>Divides the specified expressions.</summary>
		/// <param name="numerator">The numerator.</param>
		/// <param name="denominator">The denominator.</param>
		/// <returns></returns>
		public static Expression Divide(Expression numerator, Expression denominator)
		{
			return Division.Of(numerator, denominator);
		}

		/// <summary>Returns the negation of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Negate(Expression expression)
		{
			return DifferNET.Negate.Of(expression);
		}

		/// <summary>Returns the absolute value of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Abs(Expression expression)
		{
			return DifferNET.Abs.Of(expression);
		}

		/// <summary>Returns a step function of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <param name="leftValue">The left value.</param>
		/// <param name="midValue">The mid value.</param>
		/// <param name="rightValue">The right value.</param>
		/// <returns></returns>
		public static Expression Step(Expression expression,
			double leftValue, double midValue, double rightValue)
		{
			return DifferNET.Step.Of(expression, leftValue, midValue, rightValue);
		}

		/// <summary>
		/// Returns the specified expression raised to the specified power.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <param name="exponent">The exponent.</param>
		/// <returns></returns>
		public static Expression Pow(Expression expression, double exponent)
		{
			return SimplePower.Of(expression, exponent);
		}

		/// <summary>
		/// Returns the specified expression raised to the specified power.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <param name="exponent">The exponent.</param>
		/// <returns></returns>
		public static Expression Pow(Expression expression, Expression exponent)
		{
			return Power.Of(expression, exponent);
		}

		/// <summary>Returns the square root of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Sqrt(Expression expression)
		{
			return DifferNET.Sqrt.Of(expression);
		}

		/// <summary>Returns e raised to the specified expression.</summary>
		/// <param name="exponent">The expression.</param>
		/// <returns></returns>
		public static Expression Exp(Expression exponent)
		{
			return DifferNET.Exp.Of(exponent);
		}

		/// <summary>Returns the natural logarithm of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Log(Expression expression)
		{
			return Ln.Of(expression);
		}

		/// <summary>
		/// Returns the logarithm of the specified expression in the specified base.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <param name="logBase">The base of the logarithm.</param>
		/// <returns></returns>
		public static Expression Log(Expression expression, double logBase)
		{
			return SimpleLog.Of(expression, logBase);
		}

		/// <summary>
		/// Returns the logarithm of the specified expression in the specified base.
		/// </summary>
		/// <param name="expression">The expression.</param>
		/// <param name="logBase">The base of the logarithm.</param>
		/// <returns></returns>
		public static Expression Log(Expression expression, Expression logBase)
		{
			return DifferNET.Log.Of(expression, logBase);
		}

		#region Trig

		/// <summary>Returns the sine of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Sin(Expression expression)
		{
			return DifferNET.Sin.Of(expression);
		}

		/// <summary>Returns the cosine of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Cos(Expression expression)
		{
			return DifferNET.Cos.Of(expression);
		}

		/// <summary>Returns the tangent of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Tan(Expression expression)
		{
			return DifferNET.Tan.Of(expression);
		}

		/// <summary>Returns the cosecant of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Cosec(Expression expression)
		{
			return DifferNET.Cosec.Of(expression);
		}

		/// <summary>Returns the secant of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Sec(Expression expression)
		{
			return DifferNET.Sec.Of(expression);
		}

		/// <summary>Returns the cotangent of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Cot(Expression expression)
		{
			return DifferNET.Cot.Of(expression);
		}

		/// <summary>Returns the inverse sin of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Asin(Expression expression)
		{
			return ArcSin.Of(expression);
		}

		/// <summary>Returns the inverse cosine of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Acos(Expression expression)
		{
			return ArcCos.Of(expression);
		}

		/// <summary>Returns the inverse tangent of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Atan(Expression expression)
		{
			return ArcTan.Of(expression);
		}

		/// <summary>
		/// Returns the inverse tangent of the quotient of the especified expressions.
		/// </summary>
		/// <param name="y">The y.</param>
		/// <param name="x">The x.</param>
		/// <returns></returns>
		public static Expression Atan2(Expression y, Expression x)
		{
			return ArcTan2.Of(y, x);
		}

		/// <summary>Returns the inverse cosecant of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Acosec(Expression expression)
		{
			return ArcCosec.Of(expression);
		}

		/// <summary>Returns the inverse secant of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Asec(Expression expression)
		{
			return ArcSec.Of(expression);
		}

		/// <summary>Returns the inverse cotangent of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Acot(Expression expression)
		{
			return ArcCot.Of(expression);
		}
		//public static Expression Acot2(Expression y, Expression x) // ???


		/// <summary>Returns the hyperbolic sine of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Sinh(Expression expression)
		{
			return DifferNET.Sinh.Of(expression);
		}

		/// <summary>Returns the hyperbolic cosine of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Cosh(Expression expression)
		{
			return DifferNET.Cosh.Of(expression);
		}

		/// <summary>Returns the hyperbolic tangent of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Tanh(Expression expression)
		{
			return DifferNET.Tanh.Of(expression);
		}

		// TODO: Make classes for Cosech, Sech, Coth
		// As it stands, derivatives will NOT be very efficient

		/// <summary>Returns the hyperbolic cosecant of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Cosech(Expression expression)
		{
			return 1d / Sinh(expression);
		}

		/// <summary>Returns the hyperbolic secant of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Sech(Expression expression)
		{
			return 1d / Cosh(expression);
		}

		/// <summary>Returns the hyperbolic cotangent of the specified expression.</summary>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Expression Coth(Expression expression)
		{
			return 1d / Tanh(expression);
		}

		#endregion

	}
}
