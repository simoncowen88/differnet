﻿using DifferNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using DifferNET.Compiled;

namespace testbed
{
	class Program
	{
		static void Main(string[] args)
		{
			CheckCompilation();
			CheckDerivatives();
			
			//Console.WriteLine(e1 == e2);
			//Console.WriteLine(object.Equals(e1, e2));
			//Console.WriteLine(e1.Equals(e2));
			//Console.WriteLine(e2.Equals(e1));

			Console.WriteLine("---");
			Console.ReadKey();

			//PerformanceTest();
		}

		private static void CheckCompilation()
		{
			{
				var x = new Input("x");
				var y = new Input("y");

				var function = x * EMath.Cos(x + y);
				var derivatives = function.GetDerivatives(x, y);

				var compiledFunction = function.Compile(x, y);
				var compiledDerivatives = new[] { derivatives[x], derivatives[y] }.Compile(x, y);

				double value = compiledFunction.Invoke(1.3d, 4.6d);
				double[] derivative = compiledDerivatives.Invoke(1.3d, 4.6d);

				var derivative2 = new double[2];
				compiledDerivatives.Invoke(new[] {1.3d, 4.6d}, derivative);
			}
			{
				var x = new Input("x");

				var e = x * 4.3;
				var e2 = x * 3.4;

				var f1 = e.Compile(x);
				var f2 = new[] { e, e2 }.Compile(x);
				var f1_ = f1.Invoke(2d);
				var f2_ = f2.Invoke(2d);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
				f1.Invoke(3d, out f1_);
				f2.Invoke(3d, f2_);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
			}
			{
				Input x = new Input("x");
				Input y = new Input("y");

				Expression e = x * 4.3 + y;
				Expression e2 = x * 3.4 + y;

				var f1 = e.Compile(x, y);
				var f2 = new[] { e, e2 }.Compile(x, y);
				var f1_ = f1.Invoke(2d, 1d);
				var f2_ = f2.Invoke(2d, 1d);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
				f1.Invoke(new[] { 3d, 1d }, out f1_);
				f2.Invoke(new[] { 3d, 1d }, f2_);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
			}

			Console.WriteLine("------");

			{
				var model = new Model();
				Input x = model.CreateInput("x");

				Expression e = x * 4.3;
				Expression e2 = x * 3.4;

				var f1 = model.CompileForSingleInput(e);
				var f2 = model.CompileForSingleInput(e, e2);
				var f1_ = f1.Invoke(2d);
				var f2_ = f2.Invoke(2d);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
				f1.Invoke(3d, out f1_);
				f2.Invoke(3d, f2_);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
			}
			{
				var model = new Model();
				Input x = model.CreateInput("x");
				Input y = model.CreateInput("y");

				Expression e = x * 4.3 + y;
				Expression e2 = x * 3.4 + y;

				var f1 = model.Compile(e);
				var f2 = model.Compile(e, e2);
				var f1_ = f1.Invoke(2d, 1d);
				var f2_ = f2.Invoke(2d, 1d);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
				f1.Invoke(new[] { 3d, 1d }, out f1_);
				f2.Invoke(new[] { 3d, 1d }, f2_);
				Console.WriteLine(f1_);
				Console.WriteLine(f2_[0]);
				Console.WriteLine(f2_[1]);
				Console.WriteLine("---");
			}
		}

		private static void CheckDerivatives()
		{
			{
				Input x = new Input("x");
				Input y = new Input("y");

				Expression e = x * y + 3.4 * y / x;

				var derivs = e.GetDerivatives(x, y);
				Console.WriteLine(derivs[x]);
				Console.WriteLine(derivs[y]);

				var d = e.CompileDerivatives(x, y);
				var d_ = d.Invoke(2d, 1d);
				Console.WriteLine(d_[0]);
				Console.WriteLine(d_[1]);

				Console.WriteLine("---");
			}

			Console.WriteLine("------");

			{
				var model = new Model();
				Input x = model.CreateInput("x");
				Input y = model.CreateInput("y");

				Expression e = x * y + 3.4 * y / x;

				var derivs = model.GetDerivatives(e);
				Console.WriteLine(derivs[x]);
				Console.WriteLine(derivs[y]);

				var d = model.CompileDerivatives(e);
				var d_ = d.Invoke(2d, 1d);
				Console.WriteLine(d_[0]);
				Console.WriteLine(d_[1]);
			}
		}

		private static void PerformanceTest()
		{
			const int N = 1000000;
			const int K = 10;

			double tmp = 0d;

			var model = new Model();

			Input x1 = model.CreateInput("x1");
			Input x2 = model.CreateInput("x2");

			// write our expression
			Expression expression = Constant.Zero;
			for (int i = 0; i < K; i++)
			{
				Expression tmpExpr = Constant.Zero;
				for (int j = 0; j < K; j++)
				{
					tmpExpr += x2 * j;
				}
				expression += tmpExpr * x1;
			}

			// create a 'function' from it...
			IFunctionManyTo1 function = model.Compile(expression);

			var derivs = model.GetDerivatives(expression);

			IFunctionManyTo1 deriv1 = model.Compile(derivs[x1]);
			IFunctionManyTo1 deriv2 = model.Compile(derivs[x2]);

			// this takes approx 2ms
			IFunctionManyToMany multiDerivative = model.CompileDerivatives(expression);

			// these are our input values.
			double[] arg = { 72.3, 3.76 };

			var sw = Stopwatch.StartNew();

			// invoke it!
			for (int k = 0; k < N; k++)
			{
				double value = function.Invoke(arg);
				tmp += value;
			}
			//Console.WriteLine(value);

			Console.WriteLine("Invoke");
			Console.WriteLine(sw.ElapsedMilliseconds);
			Console.WriteLine("---");

			sw.Restart();

			// invoke its derivative!
			for (int k = 0; k < N; k++)
			{
				double[] derivative = multiDerivative.Invoke(arg);
				tmp += derivative[0];
			}
			//foreach (var dv in derivative)
			//	Console.WriteLine(dv);

			Console.WriteLine("InvokeDerivative");
			Console.WriteLine(sw.ElapsedMilliseconds);
			Console.WriteLine("---");

			sw.Restart();

			{
				double[] derivative = new double[2];
				// invoke it's derivative, reusing an array
				for (int k = 0; k < N; k++)
				{
					multiDerivative.Invoke(arg, derivative);
					tmp += derivative[0];
				}
			}
			//foreach (var dv in derivative)
			//	Console.WriteLine(dv);

			Console.WriteLine("InvokeDerivativeReusingArray");
			Console.WriteLine(sw.ElapsedMilliseconds);
			Console.WriteLine("---");

			sw.Restart();

			// invoke it's derivative parallel!
			Parallel.For(0, N, k =>
			{
				double[] derivative = multiDerivative.Invoke(arg);
				tmp += derivative[0];
			});
			//foreach (var dv in derivative)
			//	Console.WriteLine(dv);

			Console.WriteLine("InvokeDerivativeParallel");
			Console.WriteLine(sw.ElapsedMilliseconds);
			Console.WriteLine("---");

			sw.Restart();

			// invoke it's derivatives individually!
			for (int k = 0; k < N; k++)
			{
				double[] derivative = new double[2]
					{
						deriv1.Invoke(arg),
						deriv2.Invoke(arg),
					};
				tmp += derivative[0];
			}
			//foreach (var dv in derivative)
			//	Console.WriteLine(dv);

			Console.WriteLine("InvokeDerivativesIndividually");
			Console.WriteLine(sw.ElapsedMilliseconds);
			Console.WriteLine("---");

			sw.Restart();

			// just do it normally....

			Func<double, double, double> functionD = (d1, d2) =>
			{
				double expressionD = 0d;
				for (int i = 0; i < K; i++)
				{
					double tmpExprD = 0d;
					for (int j = 0; j < K; j++)
					{
						tmpExprD += d2 * j;
					}
					expressionD += tmpExprD * d1;
				}
				return expressionD;
			};

			for (int k = 0; k < N; k++)
			{
				double value = functionD(arg[0], arg[1]);
				tmp += value;
			}

			Console.WriteLine("Straight-Up!");
			Console.WriteLine(sw.ElapsedMilliseconds);
			Console.WriteLine("---");
		}
	}
}
