﻿using DifferNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using LX = System.Linq.Expressions;

namespace testbed
{
	public sealed class ArcCott : CustomExpression
	{
		#region Fields

		#endregion

		#region Constructors

		internal ArcCott(Expression x)
			: base(x)
		{
		}

		#endregion

		#region Properties

		internal Expression X
		{
			get { return Precedents[0]; }
		}

		#endregion

		#region Methods

		public override string ToString()
		{
			return string.Format("acott({0})", X);
		}

		#endregion

		protected override IEnumerable<ExpressionDerivative> GetDerivativeExpression()
		{
			// dy/dx arccot(x) = arctan(1/x) = -1 / (1 + x*x)

			yield return new ExpressionDerivative(X, -1d/(1d + X*X));
		}

		protected override LX.Expression GetLinqExpression(
			IMap<Expression, LX.Expression> implementations)
		{
			var oneOverX = LX.Expression.Divide(
				LX.Expression.Constant(1d, typeof (double)),
				implementations[X]);

			var methodInfo = ((Func<double, double>) Math.Atan).Method;
			return LX.Expression.Call(methodInfo, oneOverX);
		}
	}
}
