# DifferNET

A .NET library for [automatic differentiation](https://en.wikipedia.org/wiki/Automatic_differentiation) and compilation of mathematical expressions.

This library uses an `Expression` class, operator overloading, and a `Math`-like class of mathematical functions for creating mathematical expressions. 
Expressions can be differentiated, resulting in further expressions in terms of `Expression` classes and allowing higher-order derivatives to be easily calculated. 
Expression can also be easily compiled to a function, allowing for easy and fast evaluation of them.

For the differentiation of expressions, we use reverse accumulation to calculate the derivatives. This performs well, and allows to easily calculate derivatives with respect to multiple inputs.

For compiling expressions, the library uses `System.Linq.Expression` as it performs well, and is remarkably easy to use.

---

## Some example usage

### Simple differentiation and compilation

	var x = new Input("x");
	var function = x*EMath.Cos(1/x);
	var derivative = function.GetDerivative(x);

	var compiledFunction = function.Compile(x);
	var compiledDerivative = derivative.Compile(x);

	double value = compiledFunction.Invoke(1.3d);
	double serivative = compiledDerivative.Invoke(1.3d);

### Working with multiple inputs

	var x = new Input("x");
	var y = new Input("y");

	var function = x * EMath.Cos(x + y);
	var derivatives = function.GetDerivatives(x, y);

	var compiledFunction = function.Compile(x, y);
	var compiledDerivatives = new[] { derivatives[x], derivatives[y] }.Compile(x, y);

	double value = compiledFunction.Invoke(1.3d, 4.6d);
	double[] derivative = compiledDerivatives.Invoke(1.3d, 4.6d);

### Defining a custom expression
*Please note: natural log is already implemented!*

	public sealed class NaturalLog : CustomExpression
	{
		internal NaturalLog(Expression x)
			: base(x)
		{
		}

		public Expression X
		{
			get { return Precedents[0]; }
		}

		protected override IEnumerable<ExpressionDerivative> GetDerivativeExpression()
		{
			// dy/dx ln(x) = 1/x
			yield return new ExpressionDerivative(X, 1d/X);
		}

		protected override LX.Expression GetLinqExpression(
			IMap<Expression, LX.Expression> implementations)
		{
			var methodInfo = ((Func<double, double>) Math.Log).Method;
			var xCompiled = implementations[X];

			return System.Linq.Expressions.Expression.Call(methodInfo, xCompiled);
		}

		public override string ToString()
		{
			return string.Format("ln({0})", X);
		}
	}